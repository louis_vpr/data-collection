﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.ViewModels.AccountViewModels
{
    public class UserViewModel
    {
        [DisplayName("Locked?")]
        public string IsLocked { get; set; }

        [DisplayName("Admin?")]
        public string IsAdmin { get; set; }

        public int UpdatedBy_Id { get; set; }

        [DisplayName("Updated By")]
        public string UpdatedBy_Name { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedTime { get; set; }

        [DataType(DataType.Date)]
        public DateTime UpdatedTime { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
    }
}
