﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DataCollection.Models;

namespace DataCollection.ViewModels
{
    public class LookupViewModel
    {
        [Key]
        [DisplayName("ID")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Type is required")]
        [DisplayName("Type")]
        [StringLength(255)]
        public string Type { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [DisplayName("Name")]
        [StringLength(255)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Value is required")]
        [DisplayName("Value")]
        public int Value { get; set; }
    }
}