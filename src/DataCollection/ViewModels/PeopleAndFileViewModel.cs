﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DataCollection.Models;

namespace DataCollection.ViewModels
{
    public class PeopleAndFileViewModel
    {
        public People People { get; set; }
        public List<Files> Files { get; set; }
    }
}
