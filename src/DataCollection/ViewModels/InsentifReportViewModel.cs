﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using DataCollection.Models;
using FastMember;

namespace DataCollection.ViewModels
{
    public class InsentifReportViewModel
    {

        [Ordinal(0)]
        public string NamaPIC{ get; set; }

        [Ordinal(1)]
        [DisplayName("Target")]
        public int Target { get; set; }

        [Ordinal(2)]
        [DisplayName("Tidak Target")]
        public int NonTarget { get; set; }

        [Ordinal(3)]
        [DisplayName("Total Tagihan")]
        public int TotalTagihan { get; set; }

        [Ordinal(4)]
        public DateTime Tanggal { get; set; }
    }
}
