﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollection.ViewModels
{
    public class OptionViewModel
    {
        public const string TYPE_TEXT = "text";
        public const string TYPE_NUMBER = "number";
        public const string TYPE_DATE = "date";

        public string value { get; set; }
        public string dataType { get; set; }
        public string text { get; set; }

        public OptionViewModel(string value, string text, string dataType = OptionViewModel.TYPE_TEXT)
        {
            this.value = value;
            this.text = text;
            this.dataType = dataType;
        }
    }
}
