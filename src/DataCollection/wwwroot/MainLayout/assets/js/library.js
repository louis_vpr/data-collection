var LIBS = {
	// Chart libraries
	plot: [
		"../MainLayout/libs/misc/flot/jquery.flot.min.js",
		"../MainLayout/libs/misc/flot/jquery.flot.pie.min.js",
		"../MainLayout/libs/misc/flot/jquery.flot.stack.min.js",
		"../MainLayout/libs/misc/flot/jquery.flot.resize.min.js",
		"../MainLayout/libs/misc/flot/jquery.flot.curvedLines.js",
		"../MainLayout/libs/misc/flot/jquery.flot.tooltip.min.js",
		"../MainLayout/libs/misc/flot/jquery.flot.categories.min.js"
	],
	chart: [
		'../MainLayout/libs/misc/echarts/build/dist/echarts-all.js',
		'../MainLayout/libs/misc/echarts/build/dist/theme.js',
		'../MainLayout/libs/misc/echarts/build/dist/jquery.echarts.js'
	],
	circleProgress: [
		"../MainLayout/libs/bower/jquery-circle-progress/dist/circle-progress.js"
	],
	sparkline: [
		"../MainLayout/libs/misc/jquery.sparkline.min.js"
	],
	maxlength: [
		"../MainLayout/libs/bower/bootstrap-maxlength/src/bootstrap-maxlength.js"
	],
	tagsinput: [
		"../MainLayout/libs/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.css",
		"../MainLayout/libs/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js",
	],
	TouchSpin: [
		"../MainLayout/libs/bower/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css",
		"../MainLayout/libs/bower/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"
	],
	selectpicker: [
		"../MainLayout/libs/bower/bootstrap-select/dist/css/bootstrap-select.min.css",
		"../MainLayout/libs/bower/bootstrap-select/dist/js/bootstrap-select.min.js"
	],
	filestyle: [
		"../MainLayout/libs/bower/bootstrap-filestyle/src/bootstrap-filestyle.min.js"
	],
	timepicker: [
		"../MainLayout/libs/bower/bootstrap-timepicker/js/bootstrap-timepicker.js"
	],
	datetimepicker: [
		"../MainLayout/libs/bower/moment/moment.js",
		"../MainLayout/libs/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
		"../MainLayout/libs/bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"
	],
	select2: [
		"../MainLayout/libs/bower/select2/dist/css/select2.min.css",
		"../MainLayout/libs/bower/select2/dist/js/select2.full.min.js"
	],
	vectorMap: [
		"../MainLayout/libs/misc/jvectormap/jquery-jvectormap.css",
		"../MainLayout/libs/misc/jvectormap/jquery-jvectormap.min.js",
		"../MainLayout/libs/misc/jvectormap/maps/jquery-jvectormap-us-mill.js",
		"../MainLayout/libs/misc/jvectormap/maps/jquery-jvectormap-world-mill.js",
		"../MainLayout/libs/misc/jvectormap/maps/jquery-jvectormap-africa-mill.js"
	],
	summernote: [
		"../MainLayout/libs/bower/summernote/dist/summernote.css",
		"../MainLayout/libs/bower/summernote/dist/summernote.min.js"
	],
	DataTable: [
		"../MainLayout/libs/misc/datatables/datatables.min.css",
		"../MainLayout/libs/misc/datatables/datatables.min.js"
	],
	fullCalendar: [
		"../MainLayout/libs/bower/moment/moment.js",
		"../MainLayout/libs/bower/fullcalendar/dist/fullcalendar.min.css",
		"../MainLayout/libs/bower/fullcalendar/dist/fullcalendar.min.js"
	],
	dropzone: [
		"../MainLayout/libs/bower/dropzone/dist/min/dropzone.min.css",
		"../MainLayout/libs/bower/dropzone/dist/min/dropzone.min.js"
	],
	counterUp: [
		"../MainLayout/libs/bower/waypoints/lib/jquery.waypoints.min.js",
		"../MainLayout/libs/bower/counterup/jquery.counterup.min.js"
	],
	others: [
		"../MainLayout/libs/bower/switchery/dist/switchery.min.css",
		"../MainLayout/libs/bower/switchery/dist/switchery.min.js",
		"../MainLayout/libs/bower/lightbox2/dist/css/lightbox.min.css",
		"../MainLayout/libs/bower/lightbox2/dist/js/lightbox.min.js"
	]
};