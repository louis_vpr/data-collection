﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace DataCollection.Controllers
{
    [Authorize]
    public class PeopleController : ParentController
    {
        private IHostingEnvironment _hostingEnvironment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IMapper _mapper;
        private IModelRepository _repository;
        private ILogger<PeopleController> _logger;

        public PeopleController(IModelRepository Repository, IMapper Mapper, ILogger<PeopleController> Logger, 
            IHostingEnvironment hostingEnvironment)
        {
            _repository = Repository;
            _logger = Logger;
            _mapper = Mapper;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        public IActionResult ImportExcel(IFormFile file)
        {
            var errorPeople = new List<People>();
            string folderName = "UploadExcel";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file != null)
            {
                int count = 1;
                List<string> dataArray = new List<string>();
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                string sFileName = Path.GetFileNameWithoutExtension(file.FileName);
                string fullPath = Path.Combine(newPath, file.FileName);
                string fileUrl = MyHttpContext.AppBaseUrl + "/" + folderName + "/" + file.FileName;
                var fileLength = ByteSize.FromBytes(file.Length);

                while (System.IO.File.Exists(fullPath))
                {
                    string tempFileName = string.Format("{0}({1})", sFileName, count++);
                    fullPath = Path.Combine(newPath, tempFileName + sFileExtension);
                }

                ISheet sheet;
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    //Masukkan file yang didownload ke folder UPLOAD...
                    file.CopyTo(stream);
                    stream.Position = 0;
                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //Baca Excel 97-2000 formats  
                        sheet = hssfwb.GetSheetAt(0); //ambil first sheet dari workbook  
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //Baca 2007 Excel format  
                        sheet = hssfwb.GetSheetAt(0); //Ambil first sheet dari workbook   
                    }
                    IRow headerRow = sheet.GetRow(0); //Header Row
                    int cellCount = headerRow.LastCellNum;
                    for (int j = 0; j < cellCount; j++)
                    {
                        NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
                        if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                    }

                    //Looping semua Row
                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {
                        IRow row = sheet.GetRow(i);
                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

                        //Looping 1 row untuk column
                        for (int j = row.FirstCellNum; j < cellCount; j++)
                        {
                            if (row.GetCell(j) != null)
                            {
                                dataArray.Add(row.GetCell(j).ToString());
                            }
                            else
                            {
                                dataArray.Add("");
                            }
                        }
                        try
                        {
                            if (_repository.CheckPeopleData(dataArray[0], dataArray[1], dataArray[2], dataArray[3]))
                            {
                                dataArray.Clear();
                                continue;
                            }

                            ////Masukkin data array kedalam model
                            People dataPeople = new People()
                            {
                                PhoneNumber = dataArray[0],
                                Name = dataArray[1],
                                ApplicationName = dataArray[2],
                                IDNumber = dataArray[3],
                                Log = dataArray[4],
                                LogDescription = dataArray[5],
                                LogUpdateTime = DateTime.Parse(dataArray[6]),
                                DateReminder = DateTime.Parse(dataArray[7]),
                                DateOverdue = Convert.ToInt32(dataArray[8]),
                                DateRequested = DateTime.Parse(dataArray[9]),
                                Notes = dataArray[10],
                                Planning = dataArray[11],
                                TotalAmount = Convert.ToInt32(dataArray[12]),
                                TotalReturn = Convert.ToInt32(dataArray[13]),
                                PaymentLink = dataArray[14],
                            };
                            _repository.CreatePeople(dataPeople);
                            dataArray.Clear();
                        }
                        catch (Exception ex)
                        {
                            var logModel = new ActivityLog()
                            {
                                CreatedTime = DateTime.Now,
                                Description = ex.Message + ex.StackTrace,
                                Type = Utility.LogError,
                                TypeName = "ERROR",
                                Module = "People_ImportExcel"
                            };
                            _repository.CreateActivityLog(logModel);

                            var ppl = new People()
                            {
                                PhoneNumber = dataArray[0],
                                Name = dataArray[1],
                                ApplicationName = dataArray[2],
                                IDNumber = dataArray[3]
                            };
                            errorPeople.Add(ppl);
                            dataArray.Clear();
                        }
                    }

                    var peopleImport = new PeopleImport()
                    {
                        CreatedTime = DateTime.Now,
                        DownloadPath = fileUrl,
                        FilePath = fullPath,
                        FileName = file.FileName,
                        FileSize = Convert.ToInt32(fileLength.KiloBytes),
                        FileType = file.ContentType,
                        CreatedBy_Id = 0
                    };
                    _repository.CreatePeopleImport(peopleImport);
                }
            }

            if (errorPeople.Any())
            {
                ViewBag.ErrorPeople = errorPeople;
            }
            return View("Index");
        }

        [HttpPost]
        public IActionResult UploadFile(IFormFile file, int PeopleId)
        {
            string folderName = "Files";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file != null)
            {
                int count = 1;
                List<string> dataArray = new List<string>();
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                string sFileName = Path.GetFileNameWithoutExtension(file.FileName);
                string fullPath = Path.Combine(newPath, file.FileName);
                string fileUrl = MyHttpContext.AppBaseUrl + "/" + folderName + "/" + file.FileName;
                var fileLength = ByteSize.FromBytes(file.Length);

                while (System.IO.File.Exists(fullPath))
                {
                    string tempFileName = string.Format("{0}({1})", sFileName, count++);
                    fullPath = Path.Combine(newPath, tempFileName + sFileExtension);
                }

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    //Masukkan file yang didownload ke folder UPLOAD...
                    file.CopyTo(stream);
                    stream.Position = 0;
                }

                var model = new Files()
                {
                    CreatedTime = DateTime.Now,
                    DownloadPath = fileUrl,
                    FilePath = fullPath,
                    FileName = file.FileName,
                    FileSize = Convert.ToInt32(fileLength.KiloBytes),
                    FileType = file.ContentType,
                    People_ID = Convert.ToInt32(PeopleId),
                };
                _repository.CreateFiles(model);
            }
            return RedirectToAction("ViewFiles", new RouteValueDictionary(
                new { controller = "People", action = "ViewFiles", id = Convert.ToInt32(PeopleId) })
            );
        }

        [HttpPost]
        public ActionResult ExportExcel(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public ActionResult Index()
        {
            if (TempData["ErrorPeople"] != null)
            {
                ViewBag.ErrorPeople = TempData["ErrorPeople"];
                TempData.Remove("ErrorPeople");
            }
            return View();
        }

        public IActionResult Create()
        {
            ViewBag.EnableErrorSummary = false;
            return View();
        }

        [HttpPost]
        public ActionResult Create(People model)
        {
            if (ModelState.IsValid)
            {
                _repository.CreatePeople(model);
                return RedirectToAction("Index", "People");
            }
            ViewBag.EnableErrorSummary = true;
            return View(model);
        }

        public IActionResult ViewFiles(int Id)
        {
            var model = new PeopleAndFileViewModel()
            {
                People = _repository.GetPeople().FirstOrDefault(x => x.Id == Id),
                Files = _repository.GetFilesByPeople(Id)
            };

            return View(model);
        }

        public IActionResult Edit(int Id)
        {
            var model = _repository.GetPeople().FirstOrDefault(x => x.Id == Id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(People model)
        {
            if (ModelState.IsValid)
            {
                _repository.EditPeople(model);
                return RedirectToAction("Index", "People");
            }
            ViewBag.EnableErrorSummary = true;
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            List<int> All_Id = id.Split(',').Select(int.Parse).ToList();
            foreach (int Selected_Id in All_Id)
            {
                _repository.DeletePeople(Selected_Id);
            }
            return Content("");
        }

        public ActionResult Select([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetPeople();

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public ActionResult SelectFiles([DataSourceRequest]DataSourceRequest Request, int PeopleId)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetFilesByPeople(PeopleId);
            List<FileViewModel> vm = new List<FileViewModel>();

            foreach (var row in db)
            {
                FileViewModel b = _mapper.Map<Files, FileViewModel>(row);
                b.Button = "<a href = '" + row.DownloadPath + "' class='GridButtonDownload'><i class='fa fa-plus' aria-hidden='true'></i> Download</a>";
                vm.Add(b);
            }
            return Content(JsonConvert.SerializeObject(vm.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }
    }
}
