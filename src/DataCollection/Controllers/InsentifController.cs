﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using System.ComponentModel;
using FastMember;
using System.Globalization;
using System.Threading;

namespace DataCollection.Controllers
{
    [Authorize(Policy = "Admin")]
    public class InsentifController : ParentController
    {
        private IHostingEnvironment _hostingEnvironment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IMapper _mapper;
        private IModelRepository _repository;
        private ILogger<InsentifController> _logger;

        public InsentifController(IModelRepository Repository, IMapper Mapper, ILogger<InsentifController> Logger, 
            IHostingEnvironment hostingEnvironment)
        {
            _repository = Repository;
            _logger = Logger;
            _mapper = Mapper;
            _hostingEnvironment = hostingEnvironment;
        }

        //[HttpPost]
        //public IActionResult ImportExcel(IFormFile file)
        //{
        //    var errorInsentif = new List<Insentif>();
        //    string folderName = "UploadExcel";
        //    string webRootPath = _hostingEnvironment.WebRootPath;
        //    string newPath = Path.Combine(webRootPath, folderName);
        //    StringBuilder sb = new StringBuilder();
        //    if (!Directory.Exists(newPath))
        //    {
        //        Directory.CreateDirectory(newPath);
        //    }
        //    if (file != null)
        //    {
        //        int count = 1;
        //        List<string> dataArray = new List<string>();
        //        string sFileExtension = Path.GetExtension(file.FileName).ToLower();
        //        string sFileName = Path.GetFileNameWithoutExtension(file.FileName);
        //        string fullPath = Path.Combine(newPath, file.FileName);
        //        string fileUrl = MyHttpContext.AppBaseUrl + "/" + folderName + "/" + file.FileName;
        //        var fileLength = ByteSize.FromBytes(file.Length);

        //        while (System.IO.File.Exists(fullPath))
        //        {
        //            string tempFileName = string.Format("{0}({1})", sFileName, count++);
        //            fullPath = Path.Combine(newPath, tempFileName + sFileExtension);
        //        }

        //        ISheet sheet;
        //        using (var stream = new FileStream(fullPath, FileMode.Create))
        //        {
        //            //Masukkan file yang didownload ke folder UPLOAD...
        //            file.CopyTo(stream);
        //            stream.Position = 0;
        //            if (sFileExtension == ".xls")
        //            {
        //                HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //Baca Excel 97-2000 formats  
        //                sheet = hssfwb.GetSheetAt(0); //ambil first sheet dari workbook  
        //            }
        //            else
        //            {
        //                XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //Baca 2007 Excel format  
        //                sheet = hssfwb.GetSheetAt(0); //Ambil first sheet dari workbook   
        //            }
        //            IRow headerRow = sheet.GetRow(0); //Header Row
        //            int cellCount = headerRow.LastCellNum;
        //            for (int j = 0; j < cellCount; j++)
        //            {
        //                NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
        //                if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
        //            }

        //            //Looping semua Row
        //            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
        //            {
        //                IRow row = sheet.GetRow(i);
        //                if (row == null) continue;
        //                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

        //                //Looping 1 row untuk column
        //                for (int j = row.FirstCellNum; j < cellCount; j++)
        //                {
        //                    if (row.GetCell(j) != null)
        //                    {
        //                        dataArray.Add(row.GetCell(j).ToString());
        //                    }
        //                    else
        //                    {
        //                        dataArray.Add("");
        //                    }
        //                }
        //                try
        //                {
        //                    if (_repository.CheckInsentifData(dataArray[0], dataArray[1], dataArray[2], dataArray[3]))
        //                    {
        //                        dataArray.Clear();
        //                        continue;
        //                    }

        //                    ////Masukkin data array kedalam model
        //                    Insentif dataInsentif = new Insentif()
        //                    {
        //                        PhoneNumber = dataArray[0],
        //                        Name = dataArray[1],
        //                        ApplicationName = dataArray[2],
        //                        IDNumber = dataArray[3],
        //                        Log = dataArray[4],
        //                        LogDescription = dataArray[5],
        //                        LogUpdateTime = DateTime.Parse(dataArray[6]),
        //                        DateReminder = DateTime.Parse(dataArray[7]),
        //                        DateOverdue = Convert.ToInt32(dataArray[8]),
        //                        DateRequested = DateTime.Parse(dataArray[9]),
        //                        Notes = dataArray[10],
        //                        Planning = dataArray[11],
        //                        TotalAmount = Convert.ToInt32(dataArray[12]),
        //                        TotalReturn = Convert.ToInt32(dataArray[13]),
        //                        PaymentLink = dataArray[14],
        //                    };
        //                    _repository.CreateInsentif(dataInsentif);
        //                    dataArray.Clear();
        //                }
        //                catch (Exception ex)
        //                {
        //                    var logModel = new ActivityLog()
        //                    {
        //                        CreatedTime = DateTime.Now,
        //                        Description = ex.Message + ex.StackTrace,
        //                        Type = Utility.LogError,
        //                        TypeName = "ERROR",
        //                        Module = "Insentif_ImportExcel"
        //                    };
        //                    _repository.CreateActivityLog(logModel);

        //                    var ppl = new Insentif()
        //                    {
        //                        PhoneNumber = dataArray[0],
        //                        Name = dataArray[1],
        //                        ApplicationName = dataArray[2],
        //                        IDNumber = dataArray[3]
        //                    };
        //                    errorInsentif.Add(ppl);
        //                    dataArray.Clear();
        //                }
        //            }

        //            var InsentifImport = new InsentifImport()
        //            {
        //                CreatedTime = DateTime.Now,
        //                DownloadPath = fileUrl,
        //                FilePath = fullPath,
        //                FileName = file.FileName,
        //                FileSize = Convert.ToInt32(fileLength.KiloBytes),
        //                FileType = file.ContentType,
        //                CreatedBy_Id = 0
        //            };
        //            _repository.CreateInsentifImport(InsentifImport);
        //        }
        //    }

        //    if (errorInsentif.Any())
        //    {
        //        ViewBag.ErrorInsentif = errorInsentif;
        //    }
        //    return View("Index");
        //}

        //[HttpPost]
        //public IActionResult UploadFile(IFormFile file, int InsentifId)
        //{
        //    string folderName = "Files";
        //    string webRootPath = _hostingEnvironment.WebRootPath;
        //    string newPath = Path.Combine(webRootPath, folderName);
        //    StringBuilder sb = new StringBuilder();
        //    if (!Directory.Exists(newPath))
        //    {
        //        Directory.CreateDirectory(newPath);
        //    }
        //    if (file != null)
        //    {
        //        int count = 1;
        //        List<string> dataArray = new List<string>();
        //        string sFileExtension = Path.GetExtension(file.FileName).ToLower();
        //        string sFileName = Path.GetFileNameWithoutExtension(file.FileName);
        //        string fullPath = Path.Combine(newPath, file.FileName);
        //        string fileUrl = MyHttpContext.AppBaseUrl + "/" + folderName + "/" + file.FileName;
        //        var fileLength = ByteSize.FromBytes(file.Length);

        //        while (System.IO.File.Exists(fullPath))
        //        {
        //            string tempFileName = string.Format("{0}({1})", sFileName, count++);
        //            fullPath = Path.Combine(newPath, tempFileName + sFileExtension);
        //        }

        //        using (var stream = new FileStream(fullPath, FileMode.Create))
        //        {
        //            //Masukkan file yang didownload ke folder UPLOAD...
        //            file.CopyTo(stream);
        //            stream.Position = 0;
        //        }

        //        var model = new Files()
        //        {
        //            CreatedTime = DateTime.Now,
        //            DownloadPath = fileUrl,
        //            FilePath = fullPath,
        //            FileName = file.FileName,
        //            FileSize = Convert.ToInt32(fileLength.KiloBytes),
        //            FileType = file.ContentType,
        //            People_ID = Convert.ToInt32(InsentifId),
        //        };
        //        _repository.CreateFiles(model);
        //    }
        //    return RedirectToAction("ViewFiles", new RouteValueDictionary(
        //        new { controller = "Insentif", action = "ViewFiles", id = Convert.ToInt32(InsentifId) })
        //    );
        //}

        [HttpPost]
        public ActionResult ExportExcel(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public ActionResult Index()
        {
            if (TempData["ErrorInsentif"] != null)
            {
                ViewBag.ErrorInsentif = TempData["ErrorInsentif"];
                TempData.Remove("ErrorInsentif");
            }
            return View();
        }

        public IActionResult Create()
        {
            ViewBag.EnableErrorSummary = false;
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            return View();
        }

        public IActionResult Report()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Insentif model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedTime = DateTime.Now;
                model.CreatedBy_Name = User.Identity.Name;
                _repository.CreateInsentif(model);
                return RedirectToAction("Index", "Insentif");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            return View(model);
        }

        public IActionResult Edit(int Id)
        {
            var model = _repository.GetInsentif().FirstOrDefault(x => x.Id == Id);

            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Insentif model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedTime = DateTime.Now;
                model.CreatedBy_Name = User.Identity.Name;

                _repository.EditInsentif(model);
                return RedirectToAction("Index", "Insentif");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            List<int> All_Id = id.Split(',').Select(int.Parse).ToList();
            foreach (int Selected_Id in All_Id)
            {
                _repository.DeleteInsentif(Selected_Id);
            }
            return Content("");
        }

        public ActionResult Select([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetInsentif();
            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public async Task<ActionResult> GetReport(string Date)
        {
            DateTime currentDate = new DateTime();
            currentDate = Convert.ToDateTime(Date);
            currentDate = currentDate.AddDays(-(currentDate.Day - 1));
            var vmInsentifReport = new List<InsentifReportViewModel>();
            var lsInsentif = _repository.GetInsentif();
            var lsTagihan = _repository.GetTagihan()
                .Where(x => x.TanggalPembayaran.Month == currentDate.Month && x.TanggalPembayaran.Year == currentDate.Year).OrderBy(x => x.NamaPenagih)
                .ThenBy(b => b.TanggalPembayaran);

            var modelTargetAwal = _repository.GetLookup().FirstOrDefault(x => x.Type == "TargetInsentifAwal");
            decimal jumlahTagihan = 0;
            int nilaiInsentif = 0;
            var prevTagihanDate = lsTagihan.FirstOrDefault().TanggalPembayaran;
            var prevTagihanPIC = lsTagihan.FirstOrDefault().NamaPenagih;
            foreach (var tagihan in lsTagihan)
            {
                if (currentDate == tagihan.TanggalPembayaran && prevTagihanPIC == tagihan.NamaPenagih)
                {
                    jumlahTagihan += tagihan.TagihanTerbayar;

                    if (jumlahTagihan > Convert.ToDecimal(modelTargetAwal.Name))
                    {
                        var insentif = lsInsentif.Where(x =>
                            x.MinimalTagihan <= tagihan.TagihanTerbayar && x.MaksimalTagihan >= tagihan.TagihanTerbayar &&
                            x.MaksimalTelat >= tagihan.Telat).OrderBy(b => b.MaksimalTelat).FirstOrDefault();

                        if(insentif == null)
                            continue;
                        
                        nilaiInsentif += insentif.NilaiInsentif;
                    }
                }
                else
                {
                    if (jumlahTagihan < Convert.ToDecimal(modelTargetAwal.Name))
                    {
                        nilaiInsentif -= modelTargetAwal.Value;
                    }

                    else
                    {
                        nilaiInsentif += modelTargetAwal.Value;
                    }

                    var insentifReport = new InsentifReportViewModel()
                    {
                        NamaPIC = prevTagihanPIC,
                        NonTarget = 0,
                        Target = 0,
                        Tanggal = prevTagihanDate,
                        TotalTagihan = Convert.ToInt32(jumlahTagihan)
                    };

                    if (nilaiInsentif < 0)
                        insentifReport.NonTarget = nilaiInsentif;
                    else
                        insentifReport.Target = nilaiInsentif;

                    vmInsentifReport.Add(insentifReport);

                    jumlahTagihan = 0;
                    nilaiInsentif = 0;
                    currentDate = tagihan.TanggalPembayaran;
                    jumlahTagihan += tagihan.TagihanTerbayar;
                    //if (jumlahTagihan > Convert.ToDecimal(modelTargetAwal.Name))
                    //{
                    //    var insentif = lsInsentif.Where(x =>
                    //        x.MinimalTagihan <= tagihan.TagihanTerbayar && x.MaksimalTagihan >= tagihan.TagihanTerbayar &&
                    //        x.MaksimalTelat >= tagihan.Telat).OrderBy(b => b.MaksimalTelat).FirstOrDefault();

                    //    nilaiInsentif += insentif.NilaiInsentif;
                    //}
                }

                prevTagihanDate = tagihan.TanggalPembayaran;
                prevTagihanPIC = tagihan.NamaPenagih;
            }

            // Lets converts our object data to Datatable for a simplified logic.
            // Datatable is most easy way to deal with complex datatypes for easy reading and formatting.

            DataTable InsentifTable = new DataTable();
            using (var reader = ObjectReader.Create(vmInsentifReport))
            {
                InsentifTable.Load(reader);
            }

            DataTable TagihanTable = new DataTable();
            using (var reader = ObjectReader.Create(lsTagihan))
            {
                TagihanTable.Load(reader);
            }

            string webRootPath = _hostingEnvironment.WebRootPath;
            string folderName = "ReportInsentif";
            string fileName = currentDate.ToString("MMMM yyyy") + " - Insentif.xlsx";
            string URL = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, fileName);
            string newPath = Path.Combine(webRootPath, folderName);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            FileInfo file = new FileInfo(Path.Combine(newPath, fileName));
            var memoryStream = new MemoryStream();
            using (var fs = new FileStream(Path.Combine(newPath, fileName), FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook = new XSSFWorkbook();
                // Tagihan Sheet
                ISheet TagihanSheet = workbook.CreateSheet("Daftar Tagihan");

                List<String> columns = new List<string>();
                IRow row = TagihanSheet.CreateRow(0);
                int columnIndex = 0;

                foreach (System.Data.DataColumn column in TagihanTable.Columns)
                {
                    columns.Add(column.ColumnName);
                    row.CreateCell(columnIndex).SetCellValue(column.ColumnName);
                    columnIndex++;
                }

                int rowIndex = 1;
                foreach (DataRow dsrow in TagihanTable.Rows)
                {
                    row = TagihanSheet.CreateRow(rowIndex);
                    int cellIndex = 0;
                    foreach (String col in columns)
                    {
                        row.CreateCell(cellIndex).SetCellValue(dsrow[col].ToString());
                        cellIndex++;
                    }
                    rowIndex++;
                }

                // Insentif Sheet
                ISheet InsentifSheet = workbook.CreateSheet("Insentif");

                List<String> InsColumns = new List<string>();
                IRow InsRow = InsentifSheet.CreateRow(0);
                int InsColumnIndex = 0;

                foreach (System.Data.DataColumn column in InsentifTable.Columns)
                {
                    InsColumns.Add(column.ColumnName);
                    InsRow.CreateCell(InsColumnIndex).SetCellValue(column.ColumnName);
                    InsColumnIndex++;
                }

                int InsRowIndex = 1;
                foreach (DataRow dsrow in InsentifTable.Rows)
                {
                    InsRow = InsentifSheet.CreateRow(InsRowIndex);
                    int cellIndex = 0;
                    foreach (String col in InsColumns)
                    {
                        InsRow.CreateCell(cellIndex).SetCellValue(dsrow[col].ToString());
                        cellIndex++;
                    }
                    InsRowIndex++;
                }

                var culture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;

                workbook.Write(fs);
            }

            using (var fileStream = new FileStream(Path.Combine(newPath, fileName), FileMode.Open))
            {
                await fileStream.CopyToAsync(memoryStream);
            }

            memoryStream.Position = 0;
            return File(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }
    }
}
