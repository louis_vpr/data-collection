﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace DataCollection.Controllers
{
    [Authorize(Policy = "Admin")]
    public class KomisiController : ParentController
    {
        private IHostingEnvironment _hostingEnvironment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IMapper _mapper;
        private IModelRepository _repository;
        private ILogger<KomisiController> _logger;

        public KomisiController(IModelRepository Repository, IMapper Mapper, ILogger<KomisiController> Logger, 
            IHostingEnvironment hostingEnvironment)
        {
            _repository = Repository;
            _logger = Logger;
            _mapper = Mapper;
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            ViewBag.UserAplikasi = new SelectList(_repository.GetUserAplikasi(), "Name", "Name");
            ViewBag.EnableErrorSummary = false;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Komisi model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedTime = DateTime.Now;
                _repository.CreateKomisi(model);
                return RedirectToAction("Index", "Komisi");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.UserAplikasi = new SelectList(_repository.GetUserAplikasi(), "Name", "Name");
            return View(model);
        }

        public IActionResult Edit(int Id)
        {
            ViewBag.UserAplikasi = new SelectList(_repository.GetUserAplikasi(), "Name", "Name");
            var model = _repository.GetKomisi().FirstOrDefault(x => x.Id == Id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Komisi model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedTime = DateTime.Now;
                _repository.EditKomisi(model);
                return RedirectToAction("Index", "Komisi");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.UserAplikasi = new SelectList(_repository.GetUserAplikasi(), "Name", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            List<int> All_Id = id.Split(',').Select(int.Parse).ToList();
            foreach (int Selected_Id in All_Id)
            {
                _repository.DeleteKomisi(Selected_Id);
            }
            return Content("");
        }

        public ActionResult Select([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetKomisi();

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public IActionResult RefreshTagihan()
        {
            var lsTagihan = _repository.GetTagihan().Where(x => x.KomisiPercent == 0).ToList();
            foreach (var model in  lsTagihan)
            {
                var komisiModel = _repository.GetKomisi().FirstOrDefault(x => x.UserAplikasi == model.UserAplikasi);
                if (komisiModel != null)
                {
                    model.KomisiPercent = komisiModel.Value;
                    model.KomisiNominal = model.TagihanTerbayar * komisiModel.Value / 100;
                }
                _repository.EditTagihan(model);
            }
            return Json("success");
        }
    }
}
