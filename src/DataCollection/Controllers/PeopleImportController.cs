﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace DataCollection.Controllers
{
    [Authorize]
    public class PeopleImportController : ParentController
    {
        private IHostingEnvironment _hostingEnvironment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IMapper _mapper;
        private IModelRepository _repository;
        private ILogger<PeopleImportController> _logger;

        public PeopleImportController(IModelRepository Repository, IMapper Mapper, ILogger<PeopleImportController> Logger, 
            IHostingEnvironment hostingEnvironment)
        {
            _repository = Repository;
            _logger = Logger;
            _mapper = Mapper;
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Select([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetPeopleImport();
            List<PeopleImportViewModel> vm = new List<PeopleImportViewModel>();

            foreach (var row in db)
            {
                PeopleImportViewModel b = _mapper.Map<PeopleImport, PeopleImportViewModel>(row);
                b.Button = "<a href = '" + row.DownloadPath + "' class='GridButtonDownload'><i class='fa fa-plus' aria-hidden='true'></i> Download</a>";
                vm.Add(b);
            }

            return Content(JsonConvert.SerializeObject(vm.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }
    }
}
