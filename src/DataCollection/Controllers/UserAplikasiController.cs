﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace DataCollection.Controllers
{
    [Authorize(Policy = "Admin")]
    public class UserAplikasiController : ParentController
    {
        private IHostingEnvironment _hostingEnvironment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IMapper _mapper;
        private IModelRepository _repository;
        private ILogger<UserAplikasiController> _logger;

        public UserAplikasiController(IModelRepository Repository, IMapper Mapper, ILogger<UserAplikasiController> Logger, 
            IHostingEnvironment hostingEnvironment)
        {
            _repository = Repository;
            _logger = Logger;
            _mapper = Mapper;
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Id", "Name");
            ViewBag.EnableErrorSummary = false;
            return View();
        }

        [HttpPost]
        public ActionResult Create(UserAplikasi model)
        {
            if (ModelState.IsValid)
            {
                model.NamaAplikasi_Name = _repository.GetLookup().FirstOrDefault(x => x.Id == model.NamaAplikasi_Id).Name;
                _repository.CreateUserAplikasi(model);
                return RedirectToAction("Index", "UserAplikasi");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Id", "Name");
            return View(model);
        }

        public IActionResult Edit(int Id)
        {
            var model = _repository.GetUserAplikasi().FirstOrDefault(x => x.Id == Id);
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(UserAplikasi model)
        {
            if (ModelState.IsValid)
            {
                model.NamaAplikasi_Name = _repository.GetLookup().FirstOrDefault(x => x.Id == model.NamaAplikasi_Id).Name;
                _repository.EditUserAplikasi(model);
                return RedirectToAction("Index", "UserAplikasi");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            List<int> All_Id = id.Split(',').Select(int.Parse).ToList();
            foreach (int Selected_Id in All_Id)
            {
                _repository.DeleteUserAplikasi(Selected_Id);
            }
            return Content("");
        }

        public ActionResult Select([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetUserAplikasi().OrderBy(aplikasi => aplikasi.NamaAplikasi_Name).ThenBy(b=>b.Name);

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }
    }
}
