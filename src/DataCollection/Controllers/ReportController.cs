﻿using AutoMapper;
using DataCollection.Models;
using DataCollection.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollection.Controllers
{
    [Authorize(Policy = "Admin")]
    public class ReportController : ParentController
    {
        private Utility Utility = new Utility();
        private IModelRepository _repository;
        private readonly IMapper _mapper;
        private ILogger<ReportController> _logger;
        private ITemplateService _templateService;

        public ReportController(IModelRepository Repository, IMapper Mapper, ILogger<ReportController> Logger
            , ITemplateService template)
        {
            _repository = Repository;
            _mapper = Mapper;
            _logger = Logger;
            _templateService = template;
        }

        public ActionResult Harian()
        {
            return View();
        }

        public ActionResult Bulanan()
        {
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            return View();
        }

        public ActionResult Profit()
        {
            return View();
        }

        public ActionResult SelectKeuanganBilling([DataSourceRequest]DataSourceRequest Request, string Date, int Type)
        {
            DataSourceResult Result = new DataSourceResult();
            List<Billing> db = new List<Billing>();

            DateTime bulan = Convert.ToDateTime(Date);
            if (Type == Lookup.TypeKeuangan_Harian)
            {
                db = _repository.GetBilling().Where(x => x.Date.Date == bulan.Date).ToList();
            }
            else
            {
                db = _repository.GetBilling().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
            }

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public ActionResult SelectKeuanganStockEntry([DataSourceRequest]DataSourceRequest Request, string Date, int Type)
        {
            DataSourceResult Result = new DataSourceResult();

            DateTime bulan = Convert.ToDateTime(Date);
            List<StockEntry> db = new List<StockEntry>();

            if (Type == Lookup.TypeKeuangan_Harian)
            {
                db = _repository.GetStockEntry().Where(x => x.Date.Date == bulan.Date).ToList();
            }
            else
            {
                db = _repository.GetStockEntry().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
            }

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public ActionResult SelectKeuanganChart([DataSourceRequest]DataSourceRequest Request, string Date, int Type)
        {
            DataSourceResult Result = new DataSourceResult();

            DateTime bulan = Convert.ToDateTime(Date);

            List<KeuanganChartViewModel> db = new List<KeuanganChartViewModel>();
            List<StockEntry> stockEntry = new List<StockEntry>();
            List<Billing> billing = new List<Billing>();

            if (Type == Lookup.TypeKeuangan_Harian)
            {
                stockEntry = _repository.GetStockEntry().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
                billing = _repository.GetBilling().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
            }
            else
            {
                stockEntry = _repository.GetStockEntry().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
                billing = _repository.GetBilling().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
            }

            if (billing.Count > 0)
            {
                int total = 0;
                foreach (Billing a in billing)
                {
                    total += a.TotalAmountPajak;
                }
                KeuanganChartViewModel model = new KeuanganChartViewModel()
                {
                    Jumlah = total,
                    Color = "#3C0",
                    Type = "Penjualan"
                };
                db.Add(model);

            }
            else
            {
                KeuanganChartViewModel model = new KeuanganChartViewModel()
                {
                    Color = "#3C0",
                    Type = "Penjualan"
                };
                db.Add(model);

            }

            if (stockEntry.Count > 0)
            {
                int total = 0;
                foreach (StockEntry a in stockEntry)
                {
                    total += a.TotalAmountPajak;
                }
                KeuanganChartViewModel model = new KeuanganChartViewModel()
                {
                    Jumlah = total,
                    Color = "#C00",
                    Type = "Stock Entry"
                };
                db.Add(model);
            }
            else
            {
                KeuanganChartViewModel model = new KeuanganChartViewModel()
                {
                    Color = "#C00",
                    Type = "Stock Entry"
                };
                db.Add(model);
            }

            return Json(db);
        }

        public ActionResult SelectKeuanganHistoryChart([DataSourceRequest]DataSourceRequest Request, string Date, int Type)
        {
            DataSourceResult Result = new DataSourceResult();

            DateTime bulan = new DateTime();

            if (Type == Lookup.TypeKeuangan_Harian)
            {
                bulan = Convert.ToDateTime(Date).AddDays(1);
            }
            else
            {
                bulan = Convert.ToDateTime(Date).AddMonths(1);
            }

            List<KeuanganHistoryChartViewModel> db = new List<KeuanganHistoryChartViewModel>();
            List<StockEntry> stockEntry = new List<StockEntry>();
            List<Billing> billing = new List<Billing>();

            for(int i = 0; i < 3; i++)
            {

                KeuanganHistoryChartViewModel model = new KeuanganHistoryChartViewModel();

                if (Type == Lookup.TypeKeuangan_Harian)
                {
                    bulan = bulan.AddDays(-1);
                    stockEntry = _repository.GetStockEntry().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
                    billing = _repository.GetBilling().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
                    model.Date = bulan.ToString("dd");
                }
                else
                {
                    bulan = bulan.AddMonths(-1);
                    stockEntry = _repository.GetStockEntry().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
                    billing = _repository.GetBilling().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
                    model.Date = bulan.ToString("MMMM");
                }

                if (billing.Count > 0)
                {
                    int total = 0;
                    foreach (Billing a in billing)
                    {
                        total += a.TotalAmountPajak;
                    }
                    model.Penjualan = total;
                }
                else
                {
                    model.Penjualan = 0;
                }

                if (stockEntry.Count > 0)
                {
                    int total = 0;
                    foreach (StockEntry a in stockEntry)
                    {
                        total += Convert.ToInt32(a.TotalAmountPajak);
                    }
                    model.StockEntry = total;
                }
                else
                {
                    model.StockEntry = 0;
                }
                db.Add(model);
            }
            return Json(db);
        }

        public ActionResult SelectRingkasan(string Date, int Type)
        {
            DateTime bulan = Convert.ToDateTime(Date);

            RingkasanReportViewModel db = new RingkasanReportViewModel();
            List<StockEntry> stockEntry = new List<StockEntry>();
            List<Billing> billing = new List<Billing>();
            var kasKecil = new List<KasKecil>();
            var returPembelian = new List<ReturStockEntry>();
            var returPenjualan = new List<ReturPenjualan>();

            int TotalBilling = 0;
            int TotalStockEntry = 0;
            int TotalKasDebet = 0;
            int TotalKasKredit = 0;
            int TotalRetPenjualan = 0;
            int TotalRetPembelian = 0;
            int TotalStatus = 0;
            int TotalAmount = 0;

            if (Type == Lookup.TypeKeuangan_Harian)
            {
                stockEntry = _repository.GetStockEntry().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
                billing = _repository.GetBilling().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
                kasKecil = _repository.GetKasKecil().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
                returPenjualan = _repository.GetReturPenjualan().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
                returPembelian = _repository.GetReturStockEntry().Where(x => x.Date == bulan.Date && x.Date.Year == bulan.Year).ToList();
            }
            else
            {
                stockEntry = _repository.GetStockEntry().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
                billing = _repository.GetBilling().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
                kasKecil = _repository.GetKasKecil().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
                returPenjualan = _repository.GetReturPenjualan().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
                returPembelian = _repository.GetReturStockEntry().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
            }

            foreach (Billing bill in billing)
            {
                TotalBilling += bill.TotalAmountPajak;
            }

            foreach (StockEntry se in stockEntry)
            {
                TotalStockEntry += Convert.ToInt32(se.TotalAmountPajak);
            }

            foreach (var returStockEntry in returPembelian)
            {
                TotalRetPembelian += returStockEntry.TotalAmountPajak;
            }

            foreach (var retPenjualan in returPenjualan)
            {
                TotalRetPenjualan += retPenjualan.TotalAmountPajak;
            }

            foreach (var kk in kasKecil)
            {
                TotalKasDebet += kk.Debit;
                TotalKasKredit += kk.Credit;
            }

            TotalAmount = (TotalBilling + TotalKasDebet + TotalRetPembelian) -
                          (TotalStockEntry + TotalKasKredit + TotalRetPenjualan);

            db.StockEntry = TotalStockEntry.ToString("C0");
            db.Billing = TotalBilling.ToString("C0");
            db.KasDebet = TotalKasDebet.ToString("C0");
            db.KasKredit = TotalKasKredit.ToString("C0");
            db.ReturPembelian = TotalRetPembelian.ToString("C0");
            db.ReturPenjualan = TotalRetPenjualan.ToString("C0");

            if (TotalAmount > 0)
                db.Status = "LABA " + TotalAmount.ToString("C0");
            else
                db.Status = "RUGI " + TotalAmount.ToString("C0");
            return Json(db);
        }

        public ActionResult SelectRingkasanProfit(string Date, int Type)
        {
            DateTime bulan = Convert.ToDateTime(Date);
            RingkasanReportViewModel db = new RingkasanReportViewModel();
            List<Billing> billing = new List<Billing>();

            int TotalBilling = 0;
            int TotalStockEntry = 0;
            int TotalProfit = 0;
            int TotalModal = 0;
            
            billing = _repository.GetBilling().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();

            foreach (Billing bill in billing)
            {
                TotalProfit += bill.TotalProfit;
                TotalBilling += bill.TotalAmountPajak;
            }

            db.Billing = TotalBilling.ToString("C0");
            db.TotalProfit = TotalProfit.ToString("C0");
            db.TotalModal = (TotalBilling - TotalProfit).ToString("C0");
            return Json(db);
        }

        public ActionResult SelectProfit([DataSourceRequest]DataSourceRequest Request, string Date, int Type)
        {
            DataSourceResult Result = new DataSourceResult();
            List<Billing> db = new List<Billing>();

            DateTime bulan = Convert.ToDateTime(Date);
            if (Type == Lookup.TypeKeuangan_Harian)
            {
                db = _repository.GetBilling().Where(x => x.Date.Date == bulan.Date).ToList();
            }
            else
            {
                db = _repository.GetBilling().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
            }

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public ActionResult SelectProfitChart([DataSourceRequest]DataSourceRequest Request, string Date, int Type)
        {
            DataSourceResult Result = new DataSourceResult();
            DateTime bulan = Convert.ToDateTime(Date);
            List<KeuanganChartViewModel> db = new List<KeuanganChartViewModel>();
            List<Billing> billing = new List<Billing>();
            billing = _repository.GetBilling().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();

            if (billing.Count > 0)
            {
                int total = 0;
                foreach (Billing a in billing)
                {
                    total += a.TotalProfit;
                }
                KeuanganChartViewModel model = new KeuanganChartViewModel()
                {
                    Jumlah = total,
                    Color = "#3C0",
                    Type = "Profit"
                };
                db.Add(model);

            }
            else
            {
                KeuanganChartViewModel model = new KeuanganChartViewModel()
                {
                    Color = "#3C0",
                    Type = "Profit"
                };
                db.Add(model);
            }

            return Json(db);
        }

        public ActionResult SelectProfitHistoryChart([DataSourceRequest]DataSourceRequest Request, string Date, int Type)
        {
            DataSourceResult Result = new DataSourceResult();

            DateTime bulan = new DateTime();
            bulan = Convert.ToDateTime(Date).AddMonths(1);

            List<KeuanganHistoryChartViewModel> db = new List<KeuanganHistoryChartViewModel>();
            List<Billing> billing = new List<Billing>();

            for (int i = 0; i < 6; i++)
            {

                KeuanganHistoryChartViewModel model = new KeuanganHistoryChartViewModel();
                bulan = bulan.AddMonths(-1);
                billing = _repository.GetBilling().Where(x => x.Date.Month == bulan.Month && x.Date.Year == bulan.Year).ToList();
                model.Date = bulan.ToString("MMMM");

                if (billing.Count > 0)
                {
                    int total = 0;
                    foreach (Billing a in billing)
                    {
                        total += a.TotalProfit;
                    }
                    model.Penjualan = total;
                }
                else
                {
                    model.Penjualan = 0;
                }
                
                db.Add(model);
            }
            return Json(db);
        }

        public ActionResult RefreshProfit()
        {
            var result = "success";
            try
            {
                var billing = _repository.GetBilling();
                foreach (var bill in billing)
                {
                    int totalProfit = 0;
                    var billingItems = _repository.GetBillingItemsByBillingID(bill.Id);
                    foreach (var bItems in billingItems)
                    {
                        if (bItems.Profit <= 0)
                        {
                            StockEntryItems seItems = _repository.GetStockEntryItems()
                                .LastOrDefault(x => x.Items_Id == bItems.Items_Id);

                            if (seItems != null)
                            {
                                bItems.Profit = (bItems.Price - seItems.Price) * bItems.Qty;
                                if (bItems.Profit <= 0)
                                {
                                    var itemModel = _repository.GetItemByID(bItems.Items_Id);
                                    if (itemModel.SellingPrice != 0)
                                        bItems.Profit = (bItems.Price - itemModel.SellingPrice) * bItems.Qty;
                                    else
                                        bItems.Profit = 0;
                                }
                            }
                            else
                            {
                                var itemModel = _repository.GetItemByID(bItems.Items_Id);
                                if (itemModel.SellingPrice != 0)
                                    bItems.Profit = (bItems.Price - itemModel.SellingPrice) * bItems.Qty;
                                else
                                    bItems.Profit = 0;
                            }
                            var aa = _repository.EditBillingItems(bItems);
                        }
                        totalProfit += bItems.Profit;
                    }

                    bill.TotalProfit = totalProfit;
                    var bb = _repository.EditBilling(bill);
                }
            }
            catch (Exception ex)
            {
                return Json("Failed" + ex.Message);
            }
            return Json(result);
        }


    }
}
