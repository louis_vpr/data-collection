﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DataCollection.ViewModels;
using Kendo.Mvc.UI;
using DataCollection.Models;
using Microsoft.Extensions.Logging;
using AutoMapper;
using Newtonsoft.Json;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;

namespace DataCollection.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private Utility Utility = new Utility();
        private IModelRepository _repository;
        private readonly IMapper _mapper;
        private ILogger<HomeController> _logger;

        public HomeController(IModelRepository Repository, ILogger<HomeController> Logger, IMapper Mapper)
        {
            _repository = Repository;
            _logger = Logger;
            _mapper = Mapper;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public ActionResult Select_ActivityLog([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetActivityLog().ToList().OrderByDescending(x => x.Id);

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }
    }
}
