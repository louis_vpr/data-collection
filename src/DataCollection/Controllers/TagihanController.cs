﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace DataCollection.Controllers
{
    [Authorize]
    public class TagihanController : ParentController
    {
        private IHostingEnvironment _hostingEnvironment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IMapper _mapper;
        private IModelRepository _repository;
        private ILogger<TagihanController> _logger;

        public TagihanController(IModelRepository Repository, IMapper Mapper, ILogger<TagihanController> Logger, 
            IHostingEnvironment hostingEnvironment)
        {
            _repository = Repository;
            _logger = Logger;
            _mapper = Mapper;
            _hostingEnvironment = hostingEnvironment;
        }

        //[HttpPost]
        //public IActionResult ImportExcel(IFormFile file)
        //{
        //    var errorTagihan = new List<Tagihan>();
        //    string folderName = "UploadExcel";
        //    string webRootPath = _hostingEnvironment.WebRootPath;
        //    string newPath = Path.Combine(webRootPath, folderName);
        //    StringBuilder sb = new StringBuilder();
        //    if (!Directory.Exists(newPath))
        //    {
        //        Directory.CreateDirectory(newPath);
        //    }
        //    if (file != null)
        //    {
        //        int count = 1;
        //        List<string> dataArray = new List<string>();
        //        string sFileExtension = Path.GetExtension(file.FileName).ToLower();
        //        string sFileName = Path.GetFileNameWithoutExtension(file.FileName);
        //        string fullPath = Path.Combine(newPath, file.FileName);
        //        string fileUrl = MyHttpContext.AppBaseUrl + "/" + folderName + "/" + file.FileName;
        //        var fileLength = ByteSize.FromBytes(file.Length);

        //        while (System.IO.File.Exists(fullPath))
        //        {
        //            string tempFileName = string.Format("{0}({1})", sFileName, count++);
        //            fullPath = Path.Combine(newPath, tempFileName + sFileExtension);
        //        }

        //        ISheet sheet;
        //        using (var stream = new FileStream(fullPath, FileMode.Create))
        //        {
        //            //Masukkan file yang didownload ke folder UPLOAD...
        //            file.CopyTo(stream);
        //            stream.Position = 0;
        //            if (sFileExtension == ".xls")
        //            {
        //                HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //Baca Excel 97-2000 formats  
        //                sheet = hssfwb.GetSheetAt(0); //ambil first sheet dari workbook  
        //            }
        //            else
        //            {
        //                XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //Baca 2007 Excel format  
        //                sheet = hssfwb.GetSheetAt(0); //Ambil first sheet dari workbook   
        //            }
        //            IRow headerRow = sheet.GetRow(0); //Header Row
        //            int cellCount = headerRow.LastCellNum;
        //            for (int j = 0; j < cellCount; j++)
        //            {
        //                NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
        //                if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
        //            }

        //            //Looping semua Row
        //            for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
        //            {
        //                IRow row = sheet.GetRow(i);
        //                if (row == null) continue;
        //                if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;

        //                //Looping 1 row untuk column
        //                for (int j = row.FirstCellNum; j < cellCount; j++)
        //                {
        //                    if (row.GetCell(j) != null)
        //                    {
        //                        dataArray.Add(row.GetCell(j).ToString());
        //                    }
        //                    else
        //                    {
        //                        dataArray.Add("");
        //                    }
        //                }
        //                try
        //                {
        //                    if (_repository.CheckTagihanData(dataArray[0], dataArray[1], dataArray[2], dataArray[3]))
        //                    {
        //                        dataArray.Clear();
        //                        continue;
        //                    }

        //                    ////Masukkin data array kedalam model
        //                    Tagihan dataTagihan = new Tagihan()
        //                    {
        //                        PhoneNumber = dataArray[0],
        //                        Name = dataArray[1],
        //                        ApplicationName = dataArray[2],
        //                        IDNumber = dataArray[3],
        //                        Log = dataArray[4],
        //                        LogDescription = dataArray[5],
        //                        LogUpdateTime = DateTime.Parse(dataArray[6]),
        //                        DateReminder = DateTime.Parse(dataArray[7]),
        //                        DateOverdue = Convert.ToInt32(dataArray[8]),
        //                        DateRequested = DateTime.Parse(dataArray[9]),
        //                        Notes = dataArray[10],
        //                        Planning = dataArray[11],
        //                        TotalAmount = Convert.ToInt32(dataArray[12]),
        //                        TotalReturn = Convert.ToInt32(dataArray[13]),
        //                        PaymentLink = dataArray[14],
        //                    };
        //                    _repository.CreateTagihan(dataTagihan);
        //                    dataArray.Clear();
        //                }
        //                catch (Exception ex)
        //                {
        //                    var logModel = new ActivityLog()
        //                    {
        //                        CreatedTime = DateTime.Now,
        //                        Description = ex.Message + ex.StackTrace,
        //                        Type = Utility.LogError,
        //                        TypeName = "ERROR",
        //                        Module = "Tagihan_ImportExcel"
        //                    };
        //                    _repository.CreateActivityLog(logModel);

        //                    var ppl = new Tagihan()
        //                    {
        //                        PhoneNumber = dataArray[0],
        //                        Name = dataArray[1],
        //                        ApplicationName = dataArray[2],
        //                        IDNumber = dataArray[3]
        //                    };
        //                    errorTagihan.Add(ppl);
        //                    dataArray.Clear();
        //                }
        //            }

        //            var TagihanImport = new TagihanImport()
        //            {
        //                CreatedTime = DateTime.Now,
        //                DownloadPath = fileUrl,
        //                FilePath = fullPath,
        //                FileName = file.FileName,
        //                FileSize = Convert.ToInt32(fileLength.KiloBytes),
        //                FileType = file.ContentType,
        //                CreatedBy_Id = 0
        //            };
        //            _repository.CreateTagihanImport(TagihanImport);
        //        }
        //    }

        //    if (errorTagihan.Any())
        //    {
        //        ViewBag.ErrorTagihan = errorTagihan;
        //    }
        //    return View("Index");
        //}

        //[HttpPost]
        //public IActionResult UploadFile(IFormFile file, int TagihanId)
        //{
        //    string folderName = "Files";
        //    string webRootPath = _hostingEnvironment.WebRootPath;
        //    string newPath = Path.Combine(webRootPath, folderName);
        //    StringBuilder sb = new StringBuilder();
        //    if (!Directory.Exists(newPath))
        //    {
        //        Directory.CreateDirectory(newPath);
        //    }
        //    if (file != null)
        //    {
        //        int count = 1;
        //        List<string> dataArray = new List<string>();
        //        string sFileExtension = Path.GetExtension(file.FileName).ToLower();
        //        string sFileName = Path.GetFileNameWithoutExtension(file.FileName);
        //        string fullPath = Path.Combine(newPath, file.FileName);
        //        string fileUrl = MyHttpContext.AppBaseUrl + "/" + folderName + "/" + file.FileName;
        //        var fileLength = ByteSize.FromBytes(file.Length);

        //        while (System.IO.File.Exists(fullPath))
        //        {
        //            string tempFileName = string.Format("{0}({1})", sFileName, count++);
        //            fullPath = Path.Combine(newPath, tempFileName + sFileExtension);
        //        }

        //        using (var stream = new FileStream(fullPath, FileMode.Create))
        //        {
        //            //Masukkan file yang didownload ke folder UPLOAD...
        //            file.CopyTo(stream);
        //            stream.Position = 0;
        //        }

        //        var model = new Files()
        //        {
        //            CreatedTime = DateTime.Now,
        //            DownloadPath = fileUrl,
        //            FilePath = fullPath,
        //            FileName = file.FileName,
        //            FileSize = Convert.ToInt32(fileLength.KiloBytes),
        //            FileType = file.ContentType,
        //            People_ID = Convert.ToInt32(TagihanId),
        //        };
        //        _repository.CreateFiles(model);
        //    }
        //    return RedirectToAction("ViewFiles", new RouteValueDictionary(
        //        new { controller = "Tagihan", action = "ViewFiles", id = Convert.ToInt32(TagihanId) })
        //    );
        //}

        [HttpPost]
        public ActionResult ExportExcel(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public ActionResult Index()
        {
            if (TempData["ErrorTagihan"] != null)
            {
                ViewBag.ErrorTagihan = TempData["ErrorTagihan"];
                TempData.Remove("ErrorTagihan");
            }
            return View();
        }

        [Authorize(Policy = "Admin")]
        public ActionResult Bulanan()
        {
            if (TempData["ErrorTagihan"] != null)
            {
                ViewBag.ErrorTagihan = TempData["ErrorTagihan"];
                TempData.Remove("ErrorTagihan");
            }
            return View();
        }

        public IActionResult Create()
        {
            ViewBag.EnableErrorSummary = false;
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            ViewBag.MaxHariTagihan = _repository.GetLookup()
                .FirstOrDefault(x => x.Type == "GridTagihan" && x.Name == "MaxHariUser").Value;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Tagihan model)
        {
            if (ModelState.IsValid || User.IsInRole("Admin"))
            {
                var komisiModel = _repository.GetKomisi().FirstOrDefault(x => x.UserAplikasi == model.UserAplikasi);
                if (komisiModel != null)
                {
                    model.KomisiPercent = komisiModel.Value;
                    model.KomisiNominal = model.TagihanTerbayar * komisiModel.Value / 100;
                }
                if(model.NomorTelepon != null)
                    if (!model.NomorTelepon.StartsWith("0"))
                        model.NomorTelepon = "0" + model.NomorTelepon;

                model.CreatedTime = DateTime.Now;
                model.CreatedBy_Name = User.Identity.Name;
                _repository.CreateTagihan(model);

                if(model.Files != null)
                    UploadAllFiles(model.Files, model.Id);

                return RedirectToAction("Index", "Tagihan");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            ViewBag.MaxHariTagihan = _repository.GetLookup()
                .FirstOrDefault(x => x.Type == "GridTagihan" && x.Name == "MaxHariUser").Value;
            return View(model);
        }

        public IActionResult Edit(int Id)
        {
            var model = _repository.GetTagihan().FirstOrDefault(x => x.Id == Id);

            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            ViewBag.MaxHariTagihan = _repository.GetLookup()
                .FirstOrDefault(x => x.Type == "GridTagihan" && x.Name == "MaxHariUser").Value;
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Tagihan model)
        {
            if (ModelState.IsValid || User.IsInRole("Admin"))
            {
                model.CreatedTime = DateTime.Now;

                var komisiModel = _repository.GetKomisi().FirstOrDefault(x => x.UserAplikasi == model.UserAplikasi);
                if (komisiModel != null)
                {
                    model.KomisiPercent = komisiModel.Value;
                    model.KomisiNominal = model.TagihanTerbayar * komisiModel.Value / 100;
                }

                if (model.NomorTelepon != null)
                    if (!model.NomorTelepon.StartsWith("0"))
                        model.NomorTelepon = "0" + model.NomorTelepon;

                _repository.EditTagihan(model);

                if(model.Files != null)
                    UploadAllFiles(model.Files, model.Id);

                return RedirectToAction("Index", "Tagihan");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.NamaAplikasi = new SelectList(_repository.SelectLookup("NamaAplikasi"), "Name", "Name");
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            ViewBag.MaxHariTagihan = _repository.GetLookup()
                .FirstOrDefault(x => x.Type == "GridTagihan" && x.Name == "MaxHariUser").Value;
            return View(model);
        }

        [HttpPost]
        [Authorize(Policy = "Admin")]
        public ActionResult Delete(string id)
        {
            List<int> All_Id = id.Split(',').Select(int.Parse).ToList();
            foreach (int Selected_Id in All_Id)
            {
                _repository.DeleteTagihan(Selected_Id);

                var lsFile = _repository.GetTagihanFiles(Selected_Id);
                foreach (var file in lsFile)
                {
                    var model = _repository.GetTagihanFileById(file.Id);
                    System.IO.File.Delete(model.FilePath);
                    _repository.DeleteTagihanFiles(file.Id);
                }
            }
            return Content("");
        }

        [HttpPost]
        public ActionResult DeleteFiles(int id)
        {
            var model = _repository.GetTagihanFileById(id);
            System.IO.File.Delete(model.FilePath);
            _repository.DeleteTagihanFiles(id);
            return Content("");
        }

        public ActionResult Select([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetTagihan();

            if (!User.IsInRole("Admin"))
            {
                var maxHariTagihan = _repository.GetLookup()
                    .FirstOrDefault(x => x.Type == "GridTagihan" && x.Name == "MaxHariUser").Value;

                if (User.Identity.Name.Contains("dataentry"))
                {
                    db = db.Where(x => (x.TanggalPembayaran.Date == DateTime.Now.Date || x.TanggalPembayaran.Date >= DateTime.Now.AddDays(-maxHariTagihan).Date) && x.TanggalPembayaran.Year == DateTime.Now.Year)
                        .ToList();
                }
                else
                {
                    db = db.Where(x => (x.TanggalPembayaran.Date == DateTime.Now.Date || x.TanggalPembayaran.Date >= DateTime.Now.AddDays(-maxHariTagihan).Date) && x.TanggalPembayaran.Year == DateTime.Now.Year &&
                                       (x.NamaPenagih == User.Identity.Name || x.CreatedBy_Name == User.Identity.Name))
                        .ToList();
                }
            }

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public ActionResult Select_Bulanan([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetTagihan().Where(x => x.TanggalPembayaran.Month == DateTime.Now.Month && x.TanggalPembayaran.Year == DateTime.Now.Year);
            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public ActionResult SelectFiles([DataSourceRequest]DataSourceRequest Request, int TagihanId)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetTagihanFiles(TagihanId);
            List<TagihanFileViewModel> vm = new List<TagihanFileViewModel>();

            foreach (var row in db)
            {
                var b = _mapper.Map<TagihanFiles, TagihanFileViewModel>(row);
                b.ViewButton = "<a href = '" + row.DownloadPath + "' data-lightbox='gallery-2'><img class='img-responsive' src='" + row.DownloadPath + "'></a>";
                b.DeleteButton = "<a href = '/Tagihan/DeleteFiles?Id=" + row.Id+ "' id='GridButtonDeleteFiles'><i class='fa fa-trash' aria-hidden='true'></i> Delete</a>";
                vm.Add(b);
            }
            return Content(JsonConvert.SerializeObject(vm.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public JsonResult GetUserAplikasiData(string namaAplikasi)
        {
            var userAplikasi = _repository.GetUserAplikasi()
                .Where(x => x.NamaAplikasi_Name == namaAplikasi);

            return Json(userAplikasi.ToList());
        }

        private void UploadAllFiles(IEnumerable<IFormFile> allFiles, int TagihanId)
        {
            foreach (var file in allFiles)
            {
                string folderName = "TagihanFiles";
                string folderFileName = "ID-" + TagihanId;
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName,folderFileName);
                StringBuilder sb = new StringBuilder();
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file != null)
                {
                    int count = 1;
                    List<string> dataArray = new List<string>();
                    string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                    string sFileName = Path.GetFileNameWithoutExtension(file.FileName);
                    string fullPath = Path.Combine(newPath, file.FileName);
                    string fileUrl = MyHttpContext.AppBaseUrl + "/" + folderName + "/" + folderFileName + "/" + file.FileName;
                    var fileLength = ByteSize.FromBytes(file.Length);

                    while (System.IO.File.Exists(fullPath))
                    {
                        string tempFileName = string.Format("{0}({1})", sFileName, count++);
                        fullPath = Path.Combine(newPath, tempFileName + sFileExtension);
                    }

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        //Masukkan file yang didownload ke folder UPLOAD...
                        file.CopyTo(stream);
                        stream.Position = 0;
                    }

                    var fileModel = new TagihanFiles()
                    {
                        CreatedTime = DateTime.Now,
                        DownloadPath = fileUrl,
                        FilePath = fullPath,
                        FileName = file.FileName,
                        FileSize = Convert.ToInt32(fileLength.KiloBytes),
                        FileType = file.ContentType,
                        Tagihan_Id = TagihanId
                    };
                    _repository.CreateTagihanFiles(fileModel);
                }
            }
        }
    }
}
