﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;

namespace DataCollection.Controllers
{
    public class KasbonController : ParentController
    {
        private IHostingEnvironment _hostingEnvironment;
        private const string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        private readonly IMapper _mapper;
        private IModelRepository _repository;
        private ILogger<KasbonController> _logger;

        public KasbonController(IModelRepository Repository, IMapper Mapper, ILogger<KasbonController> Logger, 
            IHostingEnvironment hostingEnvironment)
        {
            _repository = Repository;
            _logger = Logger;
            _mapper = Mapper;
            _hostingEnvironment = hostingEnvironment;
        }

        public ActionResult Index()
        {
            if (TempData["ErrorText"] != null)
            {
                ViewBag.EnableErrorSummary = true;
                ViewBag.ErrorText = TempData["ErrorText"];
            }
            return View();
        }

        public IActionResult Create()
        {
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            ViewBag.EnableErrorSummary = false;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Kasbon model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedTime = DateTime.Now;
                model.CreatedBy_Name = User.Identity.Name;
                _repository.CreateKasbon(model);
                return RedirectToAction("Index", "Kasbon");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            return View(model);
        }

        public IActionResult Edit(int Id)
        {
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            var model = _repository.GetKasbon().FirstOrDefault(x => x.Id == Id);
            if (model.IsDone && !User.IsInRole("Admin"))
            {
                TempData["ErrorText"] = "Data Kasbon sudah tersetujui";
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Kasbon model)
        {
            if (ModelState.IsValid)
            {
                model.UpdatedTime = DateTime.Now;
                model.UpdatedBy_Name = User.Identity.Name;
                _repository.EditKasbon(model);
                return RedirectToAction("Index", "Kasbon");
            }
            ViewBag.EnableErrorSummary = true;
            ViewBag.NamaPIC = new SelectList(_repository.SelectLookup("NamaPIC"), "Name", "Name");
            ViewBag.TipePembayaran = new SelectList(_repository.SelectLookup("TipePembayaran"), "Name", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            List<int> All_Id = id.Split(',').Select(int.Parse).ToList();
            foreach (int Selected_Id in All_Id)
            {
                var model = _repository.GetKasbon().FirstOrDefault(x => x.Id == Selected_Id);
                if (model.IsDone && !User.IsInRole("Admin"))
                {
                    return Content("Data Kasbon sudah tersetujui");
                }
                _repository.DeleteKasbon(Selected_Id);
            }
            return Content("");
        }

        public ActionResult Select([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var db = _repository.GetKasbon();

            if (!User.IsInRole("Admin"))
            {
                db = db.Where(x => x.CreatedBy_Name == User.Identity.Name).ToList();
            }

            return Content(JsonConvert.SerializeObject(db.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        public ActionResult Inline_Edit([DataSourceRequest] DataSourceRequest request, Kasbon model)
        {
            if (model != null && ModelState.IsValid)
            {
                model.CreatedTime = DateTime.Now;
                _repository.EditKasbon(model);
                return RedirectToAction("Index", "Kasbon");
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult Inline_Delete([DataSourceRequest] DataSourceRequest request, Kasbon model)
        {
            if (model != null)
            {
                _repository.DeleteKasbon(model.Id);
            }

            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
    }
}
