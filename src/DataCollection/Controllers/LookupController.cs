﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using DataCollection.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using DataCollection.ViewModels;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.AspNetCore.Authorization;

namespace DataCollection.Controllers
{
    [Authorize(Policy = "Admin")]
    public class LookupController : ParentController
    {
        private Utility Utility = new Utility();
        private IModelRepository _repository;
        private ILogger<LookupController> _logger;

        public LookupController(IModelRepository Repository, ILogger<LookupController> logger)
        {
            _repository = Repository;
            _logger = logger;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Select([DataSourceRequest] DataSourceRequest Request)
        {
            return Json(_GetModel().ToDataSourceResult(Request));
        }

        public ActionResult Create()
        {
            List<Lookup> db = _repository.GetLookup();
            //ViewBag.Type = db.Lookup.Select(m => m.Type).Distinct();
            ViewBag.Type = db.Select(m => m.Type).Distinct();
            ViewBag.EnableErrorSummary = false;
            return View();
        }

        [HttpPost]
        public ActionResult Create(Lookup Lookup)
        {
            List<Lookup> db = _repository.GetLookup();
            //ViewBag.Type = db.Lookup.Select(m => m.Type).Distinct();
            ViewBag.Type = db.Select(m => m.Type).Distinct();

            if (ModelState.IsValid)
            {
                string result = _repository.CreateLookup(Lookup);
                if(result == "")
                {
                    ActivityLog log = new ActivityLog()
                    {
                        Type = Lookup.Log_General,
                        TypeName = "Lain-Lain",
                        Description = "Menambah Lookup " + Lookup.Name.ToString() + " pada Type " + Lookup.Type.ToString(),
                        CreatedTime = DateTime.Now
                    };
                    string ddddd = _repository.CreateActivityLog(log);
                    ViewBag.Success = true;
                    return RedirectToAction("Index", "Lookup");
                }
                _logger.LogError(result);
            }
            
            ViewBag.EnableErrorSummary = true;
            return View(Lookup);
        }

        public ActionResult Edit(int id)
        {
            List<Lookup> Lookup = _repository.GetLookup();
            Lookup data = Lookup.Where(x => x.Id == id).FirstOrDefault();
            ViewBag.Type = Lookup.Select(m => m.Type).Distinct();
            ViewBag.EnableErrorSummary = false;
            return View(data);
        }

        [HttpPost]
        public ActionResult Edit(Lookup Lookup)
        {
            if (ModelState.IsValid)
            {
                string result = _repository.EditLookup(Lookup);
                if (result == "")
                {
                    ActivityLog log = new ActivityLog()
                    {
                        Type = Lookup.Log_General,
                        TypeName = "Lain-Lain",
                        Description = "Mengubah Lookup " + Lookup.Name.ToString() + " pada Type " + Lookup.Type.ToString(),
                        CreatedTime = DateTime.Now
                    };
                    string ddddd = _repository.CreateActivityLog(log);
                    return RedirectToAction("Index", "Lookup");
                }
                _logger.LogError(result);
            }
            List<Lookup> db = _repository.GetLookup();
            ViewBag.Type = db.Select(m => m.Type).Distinct();
            ViewBag.EnableErrorSummary = true;
            return View(Lookup);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            List<int> All_Id = id.Split(',').Select(int.Parse).ToList();
            foreach (int Selected_Id in All_Id)
            {
                Lookup data = _repository.GetLookup().FirstOrDefault(x => x.Id == Selected_Id);
                ActivityLog log = new ActivityLog()
                {
                    Type = Lookup.Log_General,
                    TypeName = "Lain-Lain",
                    Description = "Menghapus Lookup " + data.Name.ToString() + " pada Type " + data.Type.ToString(),
                    CreatedTime = DateTime.Now
                };
                string result = _repository.DeleteLookup(Selected_Id);
                if (result != "")
                {
                    _logger.LogError(result);
                }
                
                string ddddd = _repository.CreateActivityLog(log);
            }
            return RedirectToAction("Index", "Lookup");
        }

        private List<LookupViewModel> _GetModel()
        {
            var AllLookup = _repository.GetLookup().OrderByDescending(x => x.Id);
            List<LookupViewModel> AllData = new List<LookupViewModel>();
            foreach (Lookup temp in AllLookup)
            {
                LookupViewModel NewData = new LookupViewModel();
                NewData.Id = temp.Id;
                NewData.Name = temp.Name;
                NewData.Type = temp.Type;
                NewData.Value = temp.Value;
                AllData.Add(NewData);
            }
            return AllData;
        }

        //protected override void Dispose(bool disposing)
        //{
        //    db.Dispose();
        //    base.Dispose(disposing);
        //}
    }
}