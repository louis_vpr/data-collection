﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DataCollection.Controllers
{
    public class AccountController : ParentController
    {
        private SignInManager<DbUser> _signInManager;
        private UserManager<DbUser> _userManager;
        private IModelRepository _repository;
        private RoleManager<IdentityRole> _roleManager;

        public AccountController(SignInManager<DbUser> signInManager, 
            IModelRepository repository,
            RoleManager<IdentityRole> roleManager,
            UserManager<DbUser> UserManager)
        {
            _signInManager = signInManager;
            _repository = repository;
            _userManager = UserManager;
            _roleManager = roleManager;
        }

        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Tagihan");
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginViewModel vm, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var signInResult = await _signInManager.PasswordSignInAsync(vm.Username,
                    vm.Password,
                    false, false);
                if (signInResult.Succeeded)
                {
                    if (string.IsNullOrWhiteSpace(returnUrl))
                    {
                        return RedirectToAction("Index", "Tagihan");
                    }
                    else
                    {
                        return Redirect(returnUrl);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Username or Password Incorrect");
                }
            }
            return View();
        }

        public ActionResult ChangePassword()
        {
            ViewBag.EnableErrorSummary = false;
            ViewBag.Success = false;
            return View();
        }

        public ActionResult ChangePasswordModel(ChangePasswordViewModel vm, bool Success, bool Error)
        {
            ViewBag.EnableErrorSummary = Error;
            ViewBag.Success = Success;
            return View("ChangePassword");
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel vm)
        {
            bool EnableErrorSummary = false;
            bool Success = false;
            if (vm.NewPassword != vm.NewPasswordConfirm)
            {
                ModelState.AddModelError("New Password & Konfirmasi berbeda", "Maaf Password baru dan konfirmasi anda tidak sama");
            }
            if (ModelState.IsValid)
            {
                var userDb = await _userManager.FindByNameAsync(User.Identity.Name);
                IdentityResult result = await _userManager.ChangePasswordAsync(userDb, vm.CurrentPassword, vm.NewPassword);
                if (result.Succeeded)
                {
                    Success = true;
                    return RedirectToAction("ChangePasswordModel", "Account", new { vm, Success = Success, Error = EnableErrorSummary });
                }
                ModelState.AddModelError("", result.Errors.ToString());
            }
            EnableErrorSummary = true;
            return RedirectToAction("ChangePasswordModel", "Account", new { vm, Success = Success, Error = EnableErrorSummary });
        }

        [HttpPost]
        public async Task<ActionResult> Reset(string UserName)
        {
            DbUser data = await _userManager.FindByNameAsync(UserName);
            var modelPW = _repository.SelectLookup("DefaultPassword");
            var newPW = "password321";
            if (modelPW.Any())
                newPW = modelPW.FirstOrDefault().Name;

            var token = await _userManager.GeneratePasswordResetTokenAsync(data);
            var resetPassResult = await _userManager.ResetPasswordAsync(data, token, newPW);
            if (resetPassResult.Succeeded)
                return Content("Password berhasil direset!");
            else
                return Content("Gagal menghapus User");
        }

        public async Task<ActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction("Login", "Account");
            }

            return RedirectToAction("Index", "Tagihan");
        }

        [Authorize(Policy = "Admin")]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Select([DataSourceRequest]DataSourceRequest Request)
        {
            DataSourceResult Result = new DataSourceResult();

            var dataUser = _repository.GetDbUsers();

            return Content(JsonConvert.SerializeObject(dataUser.ToDataSourceResult(Request), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }), "application/json");
        }

        [Authorize(Policy = "Admin")]
        public IActionResult Create()
        {
            ViewBag.EnableErrorSummary = false;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DbUser model, IFormCollection formCollection)
        {
            if (ModelState.IsValid)
            {
                string pw = formCollection["Password"].ToString();
                var result = await _userManager.CreateAsync(model, pw);
                if (result.Succeeded)
                {
                    if (model.IsAdmin)
                    {
                        if (!await _roleManager.RoleExistsAsync("Admin"))
                        {
                            // first we create Admin role    
                            var role = new IdentityRole();
                            role.Name = "Admin";
                            await _roleManager.CreateAsync(role);
                        }

                        var dataUser = await _userManager.FindByNameAsync(model.UserName);
                        await _userManager.AddToRoleAsync(dataUser, "Admin");
                    }
                    return RedirectToAction("Index");
                }
                AddErrors(result);
            }
            ViewBag.EnableErrorSummary = true;
            return View(model);
        }

        //public async Task<IActionResult> Edit(string UserName)
        //{
        //    //ApplicationUser model = db.ApplicationUser.Find(Id);
        //    var model = await _userManager.FindByNameAsync(UserName);
        //    ViewBag.EnableErrorSummary = false;
        //    return View(model);
        //}

        //[HttpPost]
        //public async Task<ActionResult> Edit(DbUser model, IFormCollection formCollection)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        string pw = formCollection["Password"].ToString();
        //        DbUser data = await _userManager.FindByNameAsync(model.UserName);
        //        data.Name = model.Name;
        //        //data.UpdatedTime = DateTime.Now;
        //        //data.UpdatedBy_Name = model.UserName;
        //        //data.IsAdmin = model.IsAdmin;

        //        //if (model.IsAdmin == true)
        //        //{
        //        //    if (!await _roleManager.RoleExistsAsync("Admin"))
        //        //    {
        //        //        // first we create Admin rool    
        //        //        var role = new IdentityRole();
        //        //        role.Name = "Admin";
        //        //        await _roleManager.CreateAsync(role);
        //        //    }

        //        //    await _userManager.AddToRoleAsync(data, "Admin");
        //        //}
        //        //else
        //        //{
        //        //    await _userManager.RemoveFromRoleAsync(data, "Admin");
        //        //}


        //        if (pw != "")
        //            data.PasswordHash = _userManager.PasswordHasher.HashPassword(data, pw);

        //        var newRecord = await _userManager.UpdateAsync(data);
        //        if (newRecord.Succeeded)
        //        {
        //            return RedirectToAction("Index");
        //        }
        //    }
        //    ViewBag.EnableErrorSummary = true;
        //    return View(model);
        //}

        [HttpPost]
        public async Task<ActionResult> Delete(string UserName)
        {
            DbUser data = await _userManager.FindByNameAsync(UserName);
            var result = await _userManager.DeleteAsync(data);
            if (result.Succeeded)
                return Content("");
            else
                return Content("Gagal menghapus User");
        }

        [HttpGet]
        public IActionResult AccessDenied(string ReturnURL = "")
        {
            return View();
        }
        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        #endregion

    }
}