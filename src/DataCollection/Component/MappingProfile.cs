﻿using AutoMapper;
using DataCollection.Models;
using DataCollection.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollection.Component
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            CreateMap<PeopleImport, PeopleImportViewModel>().ReverseMap();
            CreateMap<Files, FileViewModel>().ReverseMap();
            CreateMap<TagihanFiles, TagihanFileViewModel>().ReverseMap();
        }
        
    }
}
