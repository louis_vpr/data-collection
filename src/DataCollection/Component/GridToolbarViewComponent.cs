﻿using DataCollection.Models;
using DataCollection.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollection.Component
{
    public class GridToolbarViewComponent : ViewComponent
    {
        private readonly ModelContext _context;

        public GridToolbarViewComponent(ModelContext Context)
        {
            _context = Context;
        }

        public async Task<IViewComponentResult> InvokeAsync(String controller, List<OptionViewModel> listOption, bool enable_selectall = true, bool enable_filter = true, bool Create = true, bool Read = true, bool Update = true, bool Delete = true, bool Report = false, bool Done = false)
        {
            ViewBag.controller = controller;
            ViewBag.listOption = listOption;

            switch(controller)
            {
                case "Items":
                    //Delete = false;
                    Report = true;
                    break;

                //case "StockEntry":
                //case "Billing":
                //    Delete = false;
                //    break;

                case "ReturStockEntry":
                case "ReturPenjualan":
                    Delete = false;
                    Update = false;
                    break;

                case "Hutang":
                    Create = false;
                    Delete = false;
                    Update = false;
                    Done = true;
                    break;

                case "Conversion":
                    Delete = false;
                    Update = false;
                    break;
            }

            ViewBag.Create = Create;
            ViewBag.Read = Read;
            ViewBag.Update = Update;
            ViewBag.Delete = Delete;
            ViewBag.Report = Report;
            ViewBag.Done = Done;

            ViewBag.enable_selectall = enable_selectall;
            ViewBag.enable_filter = enable_filter;
            return View("_ToolbarGrid");
        }
    }
}
