﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class Komisi
    {
        public int Id { get; set; }

        [DisplayName("User Aplikasi")]
        public string UserAplikasi { get; set; }

        [DisplayName("Nilai Komisi")]
        public decimal Value { get; set; }

        public DateTime CreatedTime { get; set; }

        public int CreatedBy_Id { get; set; }
    }
}
