﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public interface IModelRepository
    {
        List<Lookup> GetLookup();
        string CreateLookup(Lookup model);
        string EditLookup(Lookup model);
        string DeleteLookup(int Id);
        List<Lookup> SelectLookup(string type);
        bool CheckPeopleData(string phoneNumber, string name, string applicationName, string idNumber);
        List<People> GetPeople();
        string CreatePeople(People model);
        string EditPeople(People model);
        string DeletePeople(int Id);
        List<PeopleImport> GetPeopleImport();
        string CreatePeopleImport(PeopleImport model);
        string EditPeopleImport(PeopleImport model);
        string DeletePeopleImport(int Id);
        List<Files> GetFilesByPeople(int Id);
        string CreateFiles(Files model);
        string EditFiles(Files model);
        string DeleteFiles(int Id);
        TagihanFiles GetTagihanFileById(int Id);
        List<TagihanFiles> GetTagihanFiles(int Id);
        string CreateTagihanFiles(TagihanFiles model);
        string EditTagihanFiles(TagihanFiles model);
        string DeleteTagihanFiles(int Id);
        List<Komisi> GetKomisi();
        string CreateKomisi(Komisi model);
        string EditKomisi(Komisi model);
        string DeleteKomisi(int Id);
        List<Tagihan> GetTagihan();
        string CreateTagihan(Tagihan model);
        string EditTagihan(Tagihan model);
        string DeleteTagihan(int Id);
        List<UserAplikasi> GetUserAplikasi();
        string CreateUserAplikasi(UserAplikasi model);
        string EditUserAplikasi(UserAplikasi model);
        string DeleteUserAplikasi(int Id);
        List<ActivityLog> GetActivityLog();
        string CreateActivityLog(ActivityLog model);
        List<DbUser> GetDbUsers();
        List<Insentif> GetInsentif();
        string CreateInsentif(Insentif model);
        string EditInsentif(Insentif model);
        string DeleteInsentif(int Id);
        List<Kasbon> GetKasbon();
        string CreateKasbon(Kasbon model);
        string EditKasbon(Kasbon model);
        string DeleteKasbon(int Id);
    }
}
