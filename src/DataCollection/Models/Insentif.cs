﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class Insentif
    {
        public int Id { get; set; }

        [DisplayName("Nilai Minimal Tagihan")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        public int MinimalTagihan { get; set; }

        [DisplayName("Nilai Maksimal Tagihan")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        public int MaksimalTagihan { get; set; }

        [DisplayName("Maksimal Telat")]
        public int MaksimalTelat { get; set; }

        [DisplayName("Nilai Insentif")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        public int NilaiInsentif { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy H:mm:ss}")]
        public DateTime CreatedTime { get; set; }

        public string CreatedBy_Name { get; set; }
    }
}
