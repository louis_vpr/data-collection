﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class People
    {
        public int Id { get; set; }
        [DisplayName("Nama")]
        public string Name { get; set; }
        [DisplayName("Nama Aplikasi")]
        public string ApplicationName { get; set; }
        [DisplayName("Nomor ID")]
        public string IDNumber { get; set; }
        [DisplayName("Nomor Telepon")]
        public string PhoneNumber { get; set; }
        [DisplayName("Log")]
        public string Log { get; set; }
        [DisplayName("Jenis Log")]
        public string LogDescription { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Waktu Pembaruan Log")]
        public DateTime LogUpdateTime { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Waktu Pengingat")]
        public DateTime DateReminder { get; set; }
        [DisplayName("Hari Waktu Terlewat")]
        public int DateOverdue { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("Waktu Meminta")]
        public DateTime DateRequested { get; set; }
        [DisplayName("Catatan")]
        public string Notes { get; set; }
        [DisplayName("Rencana")]
        public string Planning { get; set; }

        [DisplayFormat(DataFormatString = "{0:C0}")]
        [DisplayName("Jumlah Total")]
        public int TotalAmount { get; set; }

        [DisplayFormat(DataFormatString = "{0:C0}")]
        [DisplayName("Jumlah Pengembalian")]
        public int TotalReturn { get; set; }

        public string PaymentLink { get; set; }
        public string KtpPath { get; set; }
        public string KkPath { get; set; }

    }
}
