﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class Utility
    {
        //private ModelContext db = new ModelContext();
        private Lookup Lookup = new Lookup();
        public static string FormatNumericTelerik = "{0:n0}";
        public static string FormatNumericTelerikNew = "N";
        public static string FormatNumeric = "F";
        public static string FormatDatePicker = "dd/MM/yyyy";
        public static string FormatDateTimePicker = "dd/MM/yyyy HH:mm:ss";
        public static string FormatDate = "dd MMM yyyy";
        public static string FormatDateTime = "dd MMM yyyy HH:mm";
        public static string FormatDateTelerik = "{0:dd MMM yyyy}";
        public static string FormatDateTimeTelerik = "{0:dd MMM yyyy HH:mm:ss}";
        public static string FormatDateIBOC = "dd-MM-yyyy";
        public static string FormatDateFile = "yyyyMMddHHmmss";
        public static int DefaultPageSize = 100;
        public static int DefaultPageSize5 = 5;
        public static int DefaultPageSize_Transaction = 20;
        public static int[] DefaultPageSizeDropDown = new[] { 5, 20, 50, 100, 500, 1000 };
        public static int LogError = 1;


        public static Dictionary<string, string> GridFilter = new Dictionary<string, string>()
        {
            {"Contains","Contains"},
            {"DoesNotContain","Does Not Contain"},
            {"StartsWith","Starts With"},
            {"IsEqualTo","Is Equal To"},
            {"IsNotEqualTo","Is Not Equal To"},
            {"EndsWith","Ends With"},
            {"IsGreaterThanOrEqualTo","Greater Than"},
            {"IsLessThanOrEqualTo","Less Than"}
        };
    }
}
