﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class ModelContextSeedData
    {
        private ModelContext _context;
        private UserManager<DbUser> _userManager;
        private SignInManager<DbUser> _signInManager;
        private RoleManager<IdentityRole> _roleManager;

        public ModelContextSeedData(ModelContext Context, 
            UserManager<DbUser> UserManager,
            RoleManager<IdentityRole> roleManager,
            SignInManager<DbUser> SignInManager)
        {
            _context = Context;
            _userManager = UserManager;
            _signInManager = SignInManager;
            _roleManager = roleManager;
        }

        public async Task EnsureSeedData()
        {
            if (await _userManager.FindByEmailAsync("admin@inventory.com") == null)
            {
                var user = new DbUser()
                {
                    UserName = "admin",
                    Email = "admin@collection.com",
                    Name = "Admin",
                    IsAdmin = true
                };
                var result = await _userManager.CreateAsync(user, "password321");
                if (result.Succeeded)
                {

                }
            }
            if (await _userManager.FindByEmailAsync("user@inventory.com") == null)
            {
                var user = new DbUser()
                {
                    UserName = "user",
                    Email = "user@collection.com",
                    Name = "User",
                    IsAdmin = false
                };
                var result = await _userManager.CreateAsync(user, "password321");
                if (result.Succeeded)
                {

                }
            }
            if (!await _roleManager.RoleExistsAsync("Admin"))
            {
                // first we create Admin rool    
                var role = new IdentityRole();
                role.Name = "Admin";
                await _roleManager.CreateAsync(role);

                var dataUser = await _userManager.FindByEmailAsync("admin@collection.com");
                await _userManager.AddToRoleAsync(dataUser, "Admin");
            }
        }
    }
}
