﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class ModelRepository : IModelRepository
    {
        private ModelContext _context;
        private ILogger<ModelRepository> _logger;
        private UserManager<DbUser> _userManager;
        
        public ModelRepository(ModelContext context,
                ILogger<ModelRepository> logger,
                UserManager<DbUser> userManager)
        {
            _context = context;
            _logger = logger;
            _userManager = userManager;
        }

        public List<Lookup> GetLookup()
        {
            List<Lookup> db = _context.Lookup.ToList();
            return db;
        }

        public string CreateLookup(Lookup model)
        {
            try
            {
                _context.Lookup.Add(model);
                _context.SaveChanges();
                return "";
            }catch(Exception ex)
            {
                return ex.ToString();
            }
            
        }

        public string EditLookup(Lookup model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeleteLookup(int Id)
        {
            try
            {
                Lookup Lookup = _context.Lookup.Where(x => x.Id == Id).FirstOrDefault();
                _context.Lookup.Remove(Lookup);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<Lookup> SelectLookup(string type)
        {
            List<Lookup> db = _context.Lookup.Where(x => x.Type == type).ToList();
            return db;
        }

        public bool CheckPeopleData(string phoneNumber, string name, string applicationName, string idNumber)
        {
            return _context.People.Any(x => x.PhoneNumber == phoneNumber &&
                                            x.Name == name &&
                                            x.ApplicationName == applicationName &&
                                            x.IDNumber == idNumber);
        }
        public List<People> GetPeople()
        {
            List<People> db = _context.People.ToList();
            return db;
        }
        public string CreatePeople(People model)
        {
            try
            {
                _context.People.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditPeople(People model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeletePeople(int Id)
        {
            try
            {
                People data = _context.People.Where(x => x.Id == Id).FirstOrDefault();
                _context.People.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<PeopleImport> GetPeopleImport()
        {
            List<PeopleImport> db = _context.PeopleImport.ToList();
            return db;
        }
        public string CreatePeopleImport(PeopleImport model)
        {
            try
            {
                _context.PeopleImport.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditPeopleImport(PeopleImport model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeletePeopleImport(int Id)
        {
            try
            {
                PeopleImport data = _context.PeopleImport.Where(x => x.Id == Id).FirstOrDefault();
                _context.PeopleImport.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<Files> GetFilesByPeople(int Id)
        {
            List<Files> db = _context.Files.Where(x => x.People_ID == Id).ToList();
            return db;
        }
        public string CreateFiles(Files model)
        {
            try
            {
                _context.Files.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditFiles(Files model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeleteFiles(int Id)
        {
            try
            {
                Files data = _context.Files.Where(x => x.Id == Id).FirstOrDefault();
                _context.Files.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public TagihanFiles GetTagihanFileById(int Id)
        {
            TagihanFiles db = _context.TagihanFiles.FirstOrDefault(x => x.Id == Id);
            return db;
        }


        public List<TagihanFiles> GetTagihanFiles(int Id)
        {
            List<TagihanFiles> db = _context.TagihanFiles.Where(x => x.Tagihan_Id == Id).ToList();
            return db;
        }
        public string CreateTagihanFiles(TagihanFiles model)
        {
            try
            {
                _context.TagihanFiles.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditTagihanFiles(TagihanFiles model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeleteTagihanFiles(int Id)
        {
            try
            {
                TagihanFiles data = _context.TagihanFiles.Where(x => x.Id == Id).FirstOrDefault();
                _context.TagihanFiles.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<Komisi> GetKomisi()
        {
            List<Komisi> db = _context.Komisi.ToList();
            return db;
        }
        public string CreateKomisi(Komisi model)
        {
            try
            {
                _context.Komisi.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditKomisi(Komisi model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeleteKomisi(int Id)
        {
            try
            {
                Komisi data = _context.Komisi.Where(x => x.Id == Id).FirstOrDefault();
                _context.Komisi.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<Tagihan> GetTagihan()
        {
            List<Tagihan> db = _context.Tagihan.ToList();
            return db;
        }
        public string CreateTagihan(Tagihan model)
        {
            try
            {
                _context.Tagihan.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditTagihan(Tagihan model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeleteTagihan(int Id)
        {
            try
            {
                Tagihan data = _context.Tagihan.Where(x => x.Id == Id).FirstOrDefault();
                _context.Tagihan.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<UserAplikasi> GetUserAplikasi()
        {
            List<UserAplikasi> db = _context.UserAplikasi.ToList();
            return db;
        }
        public string CreateUserAplikasi(UserAplikasi model)
        {
            try
            {
                _context.UserAplikasi.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditUserAplikasi(UserAplikasi model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeleteUserAplikasi(int Id)
        {
            try
            {
                UserAplikasi data = _context.UserAplikasi.Where(x => x.Id == Id).FirstOrDefault();
                _context.UserAplikasi.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<ActivityLog> GetActivityLog()
        {
            List<ActivityLog> db = _context.ActivityLog.ToList();
            return db;
        }

        public string CreateActivityLog(ActivityLog model)
        {
            try
            {
                _context.ActivityLog.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<DbUser> GetDbUsers()
        {
            List<DbUser> db = _context.DbUser.ToList();
            return db;
        }

        public List<Insentif> GetInsentif()
        {
            List<Insentif> db = _context.Insentif.ToList();
            return db;
        }
        public string CreateInsentif(Insentif model)
        {
            try
            {
                _context.Insentif.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditInsentif(Insentif model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeleteInsentif(int Id)
        {
            try
            {
                Insentif data = _context.Insentif.Where(x => x.Id == Id).FirstOrDefault();
                _context.Insentif.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public List<Kasbon> GetKasbon()
        {
            List<Kasbon> db = _context.Kasbon.ToList();
            return db;
        }
        public string CreateKasbon(Kasbon model)
        {
            try
            {
                _context.Kasbon.Add(model);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string EditKasbon(Kasbon model)
        {
            try
            {
                _context.Entry(model).State = EntityState.Modified;
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public string DeleteKasbon(int Id)
        {
            try
            {
                Kasbon data = _context.Kasbon.Where(x => x.Id == Id).FirstOrDefault();
                _context.Kasbon.Remove(data);
                _context.SaveChanges();
                return "";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
