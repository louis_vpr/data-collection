﻿using FastMember;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class Tagihan
    {
        [Ordinal(0)]
        public int Id { get; set; }

        [DisplayName("Nama PIC")]
        [Required]
        [Ordinal(1)]
        public string NamaPenagih { get; set; }

        [DisplayName("Nama Aplikasi")]
        [Required]
        [Ordinal(2)]
        public string NamaAplikasi { get; set; }

        [DisplayName("Nama Nasabah")]
        [Required]
        [Ordinal(3)]
        public string NamaNasabah { get; set; }

        [DisplayName("User Aplikasi")]
        [Required]
        [Ordinal(4)]
        public string UserAplikasi { get; set; }

        [DisplayName("Nomor ID")]
        [Required]
        [Ordinal(5)]
        public string IDNumber { get; set; }

        [DisplayName("Telat")]
        [Required]
        [Ordinal(6)]
        public decimal Telat { get; set; }

        [DisplayName("Nomor Telepon")]
        [Required]
        [Ordinal(7)]
        public string NomorTelepon { get; set; }

        [DisplayName("Atas Nama")]
        [Required]
        [Ordinal(8)]
        public string NomorTeleponNama { get; set; }

        [DisplayName("Tagihan Pokok")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        [Required]
        [Ordinal(9)]
        public decimal TagihanPokok { get; set; }

        [DisplayName("Tagihan Terbayar")]
        [DisplayFormat(DataFormatString = "{0:C0}")]
        [Required]
        [Ordinal(10)]
        public decimal TagihanTerbayar { get; set; }

        [DisplayName("Tipe Pembayaran")]
        [Required]
        [Ordinal(11)]
        public string TipePembayaran { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Tanggal Pembayaran")]
        [Required]
        [Ordinal(12)]
        public DateTime TanggalPembayaran { get; set; }

        [Ordinal(13)]
        public decimal KomisiPercent { get; set; }

        [DisplayFormat(DataFormatString = "{0:C0}")]
        [Ordinal(14)]
        public decimal KomisiNominal { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy H:mm:ss}")]
        [Ordinal(15)]
        public DateTime CreatedTime { get; set; }

        [NotMapped]
        [Ordinal(16)]
        public IEnumerable<IFormFile> Files { get; set; }

        [Ordinal(17)]
        public string CreatedBy_Name { get; set; }
    }
}
