﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class Lookup
    {
        public const int City_Jakarta = 1;
        public const int City_Bandung = 2;
        public const int City_Yogyakarta = 3;

        public const int Log_General = 1;
        public const int Log_Items = 2;
        public const int Log_Customer = 3;
        public const int Log_Supplier = 4;
        public const int Log_StockEntry = 5;
        public const int Log_Billing = 6;
        public const int Log_ReturPenjualan = 7;
        public const int Log_ReturStockEntry = 8;
        public const int Log_Conversion = 9;
        public const int Log_KasKecil = 10;

        public const int TypeRetur_Pengiriman = 1;
        public const int TypeRetur_Penerimaan = 2;

        public const int TypeKeuangan_Harian = 1;
        public const int TypeKeuangan_Bulanan = 2;

        [Key]
        [DisplayName("ID")]
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Type is required")]
        [DisplayName("Type")]
        [StringLength(255)]
        public string Type { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [DisplayName("Name")]
        [StringLength(255)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Value is required")]
        [DisplayName("Value")]
        public int Value { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        public int IsAdmin { get; set; }
    }
}
