﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class ActivityLog
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string TypeName { get; set; }
        public string Module { get; set; }
        public string Description { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreatedTime { get; set; }
    }
}
