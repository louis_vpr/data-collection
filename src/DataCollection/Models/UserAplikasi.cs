﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class UserAplikasi
    {
        public int Id { get; set; }

        [ForeignKey("Lookup")]
        public int NamaAplikasi_Id { get; set; }
        [DisplayName("Nama Aplikasi")]
        public string NamaAplikasi_Name { get; set; }
        public Lookup Lookup{ get; set; }

        [DisplayName("Nama User")]
        public string Name { get; set; }
    }
}
