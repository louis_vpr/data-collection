﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class Kasbon
    {
        public int Id { get; set; }

        [DisplayName("Nama PIC")]
        public string NamaPIC { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime TanggalPengajuan { get; set; }

        [DisplayFormat(DataFormatString = "{0:C0}")]
        public decimal Nominal { get; set; }

        public string Alasan { get; set; }

        [DisplayName("Persetujuan")]
        public bool IsDone { get; set; }

        public string Pencairan{ get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? TanggalPencairan { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy H:mm:ss}")]
        public DateTime CreatedTime { get; set; }

        public string CreatedBy_Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy H:mm:ss}")]
        public DateTime? UpdatedTime { get; set; }

        public string UpdatedBy_Name { get; set; }
    }
}
