﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    public class TagihanFiles
    {
        [System.ComponentModel.DataAnnotations.Key]
        [DisplayName("ID")]
        [System.ComponentModel.DataAnnotations.ScaffoldColumn(false)]
        public int Id { get; set; }

        [ForeignKey("Tagihan")]
        public int Tagihan_Id { get; set; }
        public Tagihan Tagihan { get; set; }

        [DisplayName("File Name")]
        [System.ComponentModel.DataAnnotations.StringLength(255)]
        public string FileName { get; set; }

        [DisplayName("File Type")]
        [System.ComponentModel.DataAnnotations.StringLength(50)]
        public string FileType { get; set; }

        [DisplayName("File Size (KB)")]
        public decimal FileSize { get; set; }

        [DisplayName("File Path")]
        [System.ComponentModel.DataAnnotations.StringLength(255)]
        public string FilePath { get; set; }

        [DisplayName("Download Path")]
        [System.ComponentModel.DataAnnotations.StringLength(255)]
        public string DownloadPath { get; set; }

        [DisplayName("Created Time")]
        public System.DateTime CreatedTime { get; set; }

        [DisplayName("CreatedBy_Id")]
        public int CreatedBy_Id { get; set; }
    }
}
