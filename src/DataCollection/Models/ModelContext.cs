﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataCollection.Models
{
    /*dotnet build -r win10-x64
     *dotnet publish -c release -r win10-x64 
     * 
     * script-migration -From "last_migration_name" -To "current_migration_name"
     */

    public class ModelContext : IdentityDbContext<DbUser>
    {
        private IConfigurationRoot _config;
        public ModelContext(IConfigurationRoot config, DbContextOptions options)
            : base(options)
        {
            _config = config;
        }

        public DbSet<DbUser> DbUser { get; set; }
        public DbSet<Files> Files { get; set; }
        public DbSet<TagihanFiles> TagihanFiles { get; set; }
        public DbSet<Lookup> Lookup { get; set; }
        public DbSet<ActivityLog> ActivityLog { get; set; }
        public DbSet<People> People { get; set; }
        public DbSet<PeopleImport> PeopleImport { get; set; }
        public DbSet<Komisi> Komisi { get; set; }
        public DbSet<Tagihan> Tagihan{ get; set; }
        public DbSet<UserAplikasi> UserAplikasi{ get; set; }
        public DbSet<Insentif> Insentif { get; set; }
        public DbSet<Kasbon> Kasbon { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            //optionsBuilder.UseSqlServer(_config["ConnectionStrings:ContextConnection"]);
            optionsBuilder.UseSqlite("Data Source=DataCollection.db");
        }
    }
}
