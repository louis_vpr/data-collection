﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DataCollection.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static string GetEmail(this ClaimsPrincipal principal)
        {
            var email = principal.Claims.FirstOrDefault(c => c.Type == "Email");
            return email?.Value;
        }

        public static string GetNPP(this ClaimsPrincipal principal)
        {
            var NPP = principal.Claims.FirstOrDefault(c => c.Type == "NPP");
            return NPP?.Value;
        }
    }
}
